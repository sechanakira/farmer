package com.econetwireless.ecofarmer;

import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;

@SpringBootTest
@RunWith(SpringRunner.class)
public class EcofarmerUssdApplicationTests {

	@Autowired
	private ApplicationContext applicationContext;

	@Test
	public void contextLoads() {
	}

	@Test
	public void testBeanLoadFromContext() throws BeansException, ClassNotFoundException {
		USSDSessionService ussdSessionService = (USSDSessionService) applicationContext.getBean(
						Class.forName("com.econetwireless.ecofarmer.persistence.service.USSDSessionService"));
		USSDSession session = new USSDSession();
		session.setMsisdn("263774222407");
		session.setSessionID("1234");
		session.setSessionStartTime(LocalDateTime.now());
		ussdSessionService.saveUSSDSession(session);
		/*
		 * SMSBean smsBean = (SMSBean) applicationContext.getBean("smsBean");
		 * smsBean.sendSMS("263774222407", "Test Message");
		 */

	}

}
