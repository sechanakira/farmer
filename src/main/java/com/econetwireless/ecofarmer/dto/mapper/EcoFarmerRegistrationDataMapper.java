package com.econetwireless.ecofarmer.dto.mapper;

import com.econetwireless.ecofarmer.dto.EcoFarmerRegistrationData;
import com.econetwireless.ecofarmer.persistence.model.EcoCashCustomerData;
import com.econetwireless.ecofarmer.persistence.model.Farmer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * Created by shingirai.chanakira on 2/19/2017.
 */
@Mapper(uses = {DistrictMapper.class, ProvinceMapper.class, LanguageMapper.class}, componentModel = "spring")
public interface EcoFarmerRegistrationDataMapper {

    @Mappings({
            @Mapping(target = "msisdn", source = "farmer.msisdn"),
            @Mapping(target = "dob", source = "ecoCashCustomerData.dateOfBirth"),
            @Mapping(target = "province", expression = "java(provinceMapper.provinceIDtoName(farmer.getProvinceID()))"),
            @Mapping(target = "district", expression = "java(districtMapper.districtIDtoName(farmer.getDistrictID()))"),
            @Mapping(target = "language", expression = "java(languageMapper.languageIDtoName(farmer.getLanguageID()))")
    })
    EcoFarmerRegistrationData sourceToDestination(EcoCashCustomerData ecoCashCustomerData, Farmer farmer, ProvinceMapper provinceMapper, DistrictMapper districtMapper, LanguageMapper languageMapper);

}
