package com.econetwireless.ecofarmer.dto.mapper;

import com.econetwireless.ecofarmer.persistence.service.DistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by shingirai.chanakira on 2/19/2017.
 */
@Component
public class DistrictMapper {

    @Autowired
    private DistrictService districtService;

    public String districtIDtoName(Long districtID){
        return districtService.findDistrictByID(districtID).getDistrictName();
    }
}
