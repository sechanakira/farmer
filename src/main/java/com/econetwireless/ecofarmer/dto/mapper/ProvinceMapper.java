package com.econetwireless.ecofarmer.dto.mapper;

import com.econetwireless.ecofarmer.persistence.service.ProvinceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by shingirai.chanakira on 2/19/2017.
 */
@Component
public class ProvinceMapper {

    @Autowired
    private ProvinceService provinceService;

    public String provinceIDtoName(Long provinceID){
        return provinceService.findProvinceByID(provinceID).getProvinceName();
    }
}
