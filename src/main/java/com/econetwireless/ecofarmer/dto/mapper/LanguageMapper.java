package com.econetwireless.ecofarmer.dto.mapper;

import com.econetwireless.ecofarmer.persistence.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by shingirai.chanakira on 2/19/2017.
 */
@Component
public class LanguageMapper {

    @Autowired
    private LanguageService languageService;

    public String languageIDtoName(Long languageID){
        return languageService.findByLanguageID(languageID).getLanguageName();
    }

}
