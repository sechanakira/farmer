package com.econetwireless.ecofarmer.xml.adapter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateAdapter {

	private LocalDateAdapter() {
	}

	public static LocalDateTime unmarshal(String xmlGregorianCalendar) {
		return LocalDateTime.parse(xmlGregorianCalendar, DateTimeFormatter.ISO_DATE_TIME);
	}

	public static String marshal(LocalDateTime dateTime) {
		return dateTime.format(DateTimeFormatter.ISO_DATE_TIME);
	}

}
