package com.econetwireless.ecofarmer.persistence.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RoutingSessionData {

	@Column(name = "session_sequence")
	private Long currentSequencePosition;
	@Column(name = "application_name")
	private String applicationName;

	public Long getCurrentSequencePosition() {
		return currentSequencePosition;
	}

	public void setCurrentSequencePosition(Long currentSequencePosition) {
		this.currentSequencePosition = currentSequencePosition;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

}
