package com.econetwireless.ecofarmer.persistence.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

@Entity
@Table(name = "esoko_response")
public class EsokoAPICallResponse {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "status")
	private String status;
	@Column(name = "description")
	private String description;
	@Column(name = "response_time")
	private LocalDateTime responseTime;
	@Column(name = "msisdn")
	private String msisdn;
	@Column(name = "reference")
	private String reference;

	public void setId(Long id) {
		this.id = id;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@PrePersist
	private void setResponseTime() {
		this.responseTime = LocalDateTime.now();
	}

	public void setResponseTime(LocalDateTime responseTime) {
		this.responseTime = responseTime;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

}
