package com.econetwireless.ecofarmer.persistence.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class LanguageSelectionSessionData {

	@Enumerated(EnumType.STRING)
	@Column(name = "language_selection_stage")
	private USSDSessionStage stage;

	private String newLanguageSelection;

	public USSDSessionStage getStage() {
		return stage;
	}

	public void setStage(USSDSessionStage stage) {
		this.stage = stage;
	}

	public String getNewLanguageSelection() {
		return newLanguageSelection;
	}

	public void setNewLanguageSelection(String newLanguageSelection) {
		this.newLanguageSelection = newLanguageSelection;
	}

}
