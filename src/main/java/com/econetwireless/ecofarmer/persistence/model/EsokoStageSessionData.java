package com.econetwireless.ecofarmer.persistence.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class EsokoStageSessionData {

	@Enumerated(EnumType.STRING)
	@Column(name = "esoko_stage")
	private USSDSessionStage stage;
	@Column(name = "selected_esoko_option")
	private Long selectedEsokoOption;
	@Enumerated(EnumType.STRING)
	@Column(name = "esoko_subscription_stage")
	private USSDSessionStage esokoSubscriptionStage;
	@Column(name = "esoko_selected_billing_option")
	private Long selectedBillingOption;
	@Column(name = "esoko_selected_payment_option")
	private Long selectedPaymentOption;
	@Column(name = "selected_tip_id")
	private Long selectedTipId;

	public USSDSessionStage getStage() {
		return stage;
	}

	public void setStage(USSDSessionStage stage) {
		this.stage = stage;
	}

	public Long getSelectedEsokoOption() {
		return selectedEsokoOption;
	}

	public void setSelectedEsokoOption(Long selectedEsokoOption) {
		this.selectedEsokoOption = selectedEsokoOption;
	}

	public USSDSessionStage getEsokoSubscriptionStage() {
		return esokoSubscriptionStage;
	}

	public void setEsokoSubscriptionStage(USSDSessionStage esokoSubscriptionStage) {
		this.esokoSubscriptionStage = esokoSubscriptionStage;
	}

	public Long getSelectedBillingOption() {
		return selectedBillingOption;
	}

	public void setSelectedBillingOption(Long selectedBillingOption) {
		this.selectedBillingOption = selectedBillingOption;
	}

	public Long getSelectedPaymentOption() {
		return selectedPaymentOption;
	}

	public void setSelectedPaymentOption(Long selectedPaymentOption) {
		this.selectedPaymentOption = selectedPaymentOption;
	}

	public Long getSelectedTipId() {
		return selectedTipId;
	}

	public void setSelectedTipId(Long selectedTipId) {
		this.selectedTipId = selectedTipId;
	}

}
