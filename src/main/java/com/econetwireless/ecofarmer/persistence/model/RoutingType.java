package com.econetwireless.ecofarmer.persistence.model;

public enum RoutingType {
	SDP, HXC, IS_GATEWAY
}
