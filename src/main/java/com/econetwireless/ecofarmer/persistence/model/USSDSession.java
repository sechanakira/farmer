package com.econetwireless.ecofarmer.persistence.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ussd_session")
public class USSDSession {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "msisdn")
	private String msisdn;
	@Column(name = "session_id")
	private String sessionID;
	@Column(name = "session_start_time")
	private LocalDateTime sessionStartTime;
	@Enumerated(EnumType.STRING)
	@Column(name = "session_stage")
	private USSDSessionStage nextStage;
	@Embedded
	private RegistationStageSessionData registrationStage;
	@Embedded
	private ProvinceSelectionSessionData provinceSelectionSessionData;
	@Embedded
	private DistrictSelectionSessionData districtSelectionSessionData;
	@Embedded
	private WardSelectionSessionData wardSelectionSessionData;
	@Embedded
	private FarmTypeSelectionSessionData farmTypeSelectionSessionData;
	@Embedded
	private LanguageSelectionSessionData languageSelectionSessionData;
	@Embedded
	private RoutingSessionData routingSessionData;
	@Embedded
	private EcoFarmerServicesSessionData ecoFarmerServicesSessionData;
	@Embedded
	private InformationServicesSessionData informationServicesSessionData;
	@Embedded
	private HelpSessionData helpSessionData;
	@Embedded
	private EcoFarmerClubsSessionData ecoFarmerClubsSessionData;
	@Embedded
	private EsokoStageSessionData esokoStageSessionData;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public USSDSessionStage getUssdSessionStage() {
		return nextStage;
	}

	public void setUssdSessionStage(USSDSessionStage ussdSessionStage) {
		this.nextStage = ussdSessionStage;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public USSDSessionStage getNextStage() {
		return nextStage;
	}

	public void setNextStage(USSDSessionStage nextStage) {
		this.nextStage = nextStage;
	}

	public LocalDateTime getSessionStartTime() {
		return sessionStartTime;
	}

	public void setSessionStartTime(LocalDateTime sessionStartTime) {
		this.sessionStartTime = sessionStartTime;
	}

	public RegistationStageSessionData getRegistrationStage() {
		return registrationStage;
	}

	public void setRegistrationStage(RegistationStageSessionData registrationStage) {
		this.registrationStage = registrationStage;
	}

	public ProvinceSelectionSessionData getProvinceSelectionSessionData() {
		return provinceSelectionSessionData;
	}

	public void setProvinceSelectionSessionData(ProvinceSelectionSessionData provinceSelectionSessionData) {
		this.provinceSelectionSessionData = provinceSelectionSessionData;
	}

	public DistrictSelectionSessionData getDistrictSelectionSessionData() {
		return districtSelectionSessionData;
	}

	public void setDistrictSelectionSessionData(DistrictSelectionSessionData districtSelectionSessionData) {
		this.districtSelectionSessionData = districtSelectionSessionData;
	}

	public WardSelectionSessionData getWardSelectionSessionData() {
		return wardSelectionSessionData;
	}

	public void setWardSelectionSessionData(WardSelectionSessionData wardSelectionSessionData) {
		this.wardSelectionSessionData = wardSelectionSessionData;
	}

	public FarmTypeSelectionSessionData getFarmTypeSelectionSessionData() {
		return farmTypeSelectionSessionData;
	}

	public void setFarmTypeSelectionSessionData(FarmTypeSelectionSessionData farmTypeSelectionSessionData) {
		this.farmTypeSelectionSessionData = farmTypeSelectionSessionData;
	}

	public LanguageSelectionSessionData getLanguageSelectionSessionData() {
		return languageSelectionSessionData;
	}

	public void setLanguageSelectionSessionData(LanguageSelectionSessionData languageSelectionSessionData) {
		this.languageSelectionSessionData = languageSelectionSessionData;
	}

	public RoutingSessionData getRoutingSessionData() {
		return routingSessionData;
	}

	public void setRoutingSessionData(RoutingSessionData routingSessionData) {
		this.routingSessionData = routingSessionData;
	}

	public EcoFarmerServicesSessionData getEcoFarmerServicesSessionData() {
		return ecoFarmerServicesSessionData;
	}

	public void setEcoFarmerServicesSessionData(EcoFarmerServicesSessionData ecoFarmerServicesSessionData) {
		this.ecoFarmerServicesSessionData = ecoFarmerServicesSessionData;
	}

	public InformationServicesSessionData getInformationServicesSessionData() {
		return informationServicesSessionData;
	}

	public void setInformationServicesSessionData(InformationServicesSessionData informationServicesSessionData) {
		this.informationServicesSessionData = informationServicesSessionData;
	}

	public HelpSessionData getHelpSessionData() {
		return helpSessionData;
	}

	public void setHelpSessionData(HelpSessionData helpSessionData) {
		this.helpSessionData = helpSessionData;
	}

	public EcoFarmerClubsSessionData getEcoFarmerClubsSessionData() {
		return ecoFarmerClubsSessionData;
	}

	public void setEcoFarmerClubsSessionData(EcoFarmerClubsSessionData ecoFarmerClubsSessionData) {
		this.ecoFarmerClubsSessionData = ecoFarmerClubsSessionData;
	}

	public EsokoStageSessionData getEsokoStageSessionData() {
		return esokoStageSessionData;
	}

	public void setEsokoStageSessionData(EsokoStageSessionData esokoStageSessionData) {
		this.esokoStageSessionData = esokoStageSessionData;
	}

}
