package com.econetwireless.ecofarmer.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ussd_menu")
public class USSDMenu {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "menu_id")
	private Long menuId;
	@Column(name = "display_name")
	private String displayName;
	@Column(name = "display_number")
	private Long displayNumber;
	@Column(name = "handler_class")
	private String handlerClass;
	@Enumerated(EnumType.STRING)
	@Column(name = "ussd_session_stage")
	private USSDSessionStage ussdSessionStage;

	public Long getMenuId() {
		return menuId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public String getHandlerClass() {
		return handlerClass;
	}

	public Long getDisplayNumber() {
		return displayNumber;
	}

	public USSDSessionStage getUssdSessionStage() {
		return ussdSessionStage;
	}

}
