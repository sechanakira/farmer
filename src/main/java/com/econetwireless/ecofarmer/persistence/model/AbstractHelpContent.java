package com.econetwireless.ecofarmer.persistence.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractHelpContent {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "help_question")
	private String helpQuestion;
	@Column(name = "help_answer")
	private String helpAnswer;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHelpQuestion() {
		return helpQuestion;
	}

	public void setHelpQuestion(String helpQuestion) {
		this.helpQuestion = helpQuestion;
	}

	public String getHelpAnswer() {
		return helpAnswer;
	}

	public void setHelpAnswer(String helpAnswer) {
		this.helpAnswer = helpAnswer;
	}

}
