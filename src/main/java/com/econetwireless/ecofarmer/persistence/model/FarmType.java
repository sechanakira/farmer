package com.econetwireless.ecofarmer.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "farm_type")
public class FarmType {

	@Id
	@Column(name = "farm_type_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long farmTypeID;
	@Column(name = "farm_type_name")
	private String farmTypeName;

	public Long getFarmTypeID() {
		return farmTypeID;
	}

	public String getFarmTypeName() {
		return farmTypeName;
	}

}
