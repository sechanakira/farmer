package com.econetwireless.ecofarmer.persistence.model;

public enum Gender {
	MALE, FEMALE
}
