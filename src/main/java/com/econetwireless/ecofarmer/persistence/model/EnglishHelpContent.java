package com.econetwireless.ecofarmer.persistence.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "english_help_content")
public class EnglishHelpContent extends AbstractHelpContent {

}
