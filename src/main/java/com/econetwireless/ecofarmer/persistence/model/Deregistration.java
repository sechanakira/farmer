package com.econetwireless.ecofarmer.persistence.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.econetwireless.ecofarmer.persistence.converter.LocalDateTimeAttributeConverter;

@Entity
@Table(name = "ecofarmer_deregistration")
public class Deregistration {

	@Id
	@Column(name = "msisdn")
	private String msisdn;
	@Column(name = "ward_number")
	private Long wardNumber;
	@Column(name = "province_id")
	private Long provinceID;
	@Column(name = "district_id")
	private Long districtID;
	@Column(name = "farm_type_id")
	private Long farmTypeID;
	@Column(name = "language_id")
	private Long languageID;
	@Column(name = "registration_date")
	@Convert(converter = LocalDateTimeAttributeConverter.class)
	private LocalDateTime registrationDate;
	@Column(name = "de_registration_date")
	@Convert(converter = LocalDateTimeAttributeConverter.class)
	private LocalDateTime deRegistrationDate;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public Long getWardNumber() {
		return wardNumber;
	}

	public void setWardNumber(Long wardNumber) {
		this.wardNumber = wardNumber;
	}

	public Long getProvinceID() {
		return provinceID;
	}

	public void setProvinceID(Long provinceID) {
		this.provinceID = provinceID;
	}

	public Long getDistrictID() {
		return districtID;
	}

	public void setDistrictID(Long districtID) {
		this.districtID = districtID;
	}

	public Long getFarmTypeID() {
		return farmTypeID;
	}

	public void setFarmTypeID(Long farmTypeID) {
		this.farmTypeID = farmTypeID;
	}

	public Long getLanguageID() {
		return languageID;
	}

	public void setLanguageID(Long languageID) {
		this.languageID = languageID;
	}

	public LocalDateTime getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(LocalDateTime registrationDate) {
		this.registrationDate = registrationDate;
	}

	public LocalDateTime getDeRegistrationDate() {
		return deRegistrationDate;
	}

	public void setDeRegistrationDate(LocalDateTime deRegistrationDate) {
		this.deRegistrationDate = deRegistrationDate;
	}

}
