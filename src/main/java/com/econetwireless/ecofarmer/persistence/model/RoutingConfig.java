package com.econetwireless.ecofarmer.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "routing_config")
public class RoutingConfig {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Enumerated(EnumType.STRING)
	@Column(name = "routing_type")
	private RoutingType routingType;
	@Column(name = "routing_url")
	private String routingURL;
	@Column(name = "application_name")
	private String applicationName;

	public Long getId() {
		return id;
	}

	public RoutingType getRoutingType() {
		return routingType;
	}

	public String getRoutingURL() {
		return routingURL;
	}

	public String getApplicationName() {
		return applicationName;
	}

}
