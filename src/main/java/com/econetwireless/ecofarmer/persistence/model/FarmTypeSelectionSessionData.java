package com.econetwireless.ecofarmer.persistence.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class FarmTypeSelectionSessionData {

	@Enumerated(EnumType.STRING)
	@Column(name = "farm_type_selection_stage")
	private USSDSessionStage stage;

	public USSDSessionStage getStage() {
		return stage;
	}

	public void setStage(USSDSessionStage stage) {
		this.stage = stage;
	}
}
