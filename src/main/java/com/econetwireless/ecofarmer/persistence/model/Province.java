package com.econetwireless.ecofarmer.persistence.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "province")
public class Province {

	@Id
	@Column(name = "province_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long provinceID;
	@Column(name = "province_name")
	private String provinceName;
	@OneToMany(mappedBy = "provinceID")
	private List<District> districts;

	public Long getProvinceID() {
		return provinceID;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public List<District> getDistricts() {
		return districts;
	}

}
