package com.econetwireless.ecofarmer.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "api_key")
public class APIKey {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "application_name")
	private String applicationName;
	@Column(name = "api_key")
	private String apiKey;

	public Long getId() {
		return id;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public String getApiKey() {
		return apiKey;
	}

}
