package com.econetwireless.ecofarmer.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "district")
public class District {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "district_id")
	private Long districtID;
	@Column(name = "province_id")
	private Long provinceID;
	@Column(name = "district_name")
	private String districtName;
	@Column(name = "number_of_wards")
	private Long numberOfWards;

	public Long getDistrictID() {
		return districtID;
	}

	public Long getProvinceID() {
		return provinceID;
	}

	public String getDistrictName() {
		return districtName;
	}

	public Long getNumberOfWards() {
		return numberOfWards;
	}

}
