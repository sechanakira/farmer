package com.econetwireless.ecofarmer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.econetwireless.ecofarmer.persistence.model.EnglishHelpContent;

public interface EnglishHelpContentRepository extends JpaRepository<EnglishHelpContent, Long> {

}
