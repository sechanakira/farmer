package com.econetwireless.ecofarmer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.econetwireless.ecofarmer.persistence.model.EsokoBillingOption;

public interface EsokoBillingOptionRepository extends JpaRepository<EsokoBillingOption, Long> {

}
