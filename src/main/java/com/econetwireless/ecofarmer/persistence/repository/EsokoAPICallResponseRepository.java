package com.econetwireless.ecofarmer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.econetwireless.ecofarmer.persistence.model.EsokoAPICallResponse;

public interface EsokoAPICallResponseRepository extends JpaRepository<EsokoAPICallResponse, Long> {

}
