package com.econetwireless.ecofarmer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.econetwireless.ecofarmer.persistence.model.EsokoPaymentMethod;

public interface EsokoPaymentMethodRepository extends JpaRepository<EsokoPaymentMethod, Long> {

}
