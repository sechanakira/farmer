package com.econetwireless.ecofarmer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.econetwireless.ecofarmer.persistence.model.APIKey;

public interface APIKeyRepository extends JpaRepository<APIKey, Long> {

	APIKey findByApplicationName(String applicationName);
}
