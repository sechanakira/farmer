package com.econetwireless.ecofarmer.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.econetwireless.ecofarmer.persistence.model.USSDMenu;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;

public interface USSDMenuRepository extends JpaRepository<USSDMenu, Long> {

	List<USSDMenu> findByUssdSessionStage(USSDSessionStage ussdSessionStage);

	USSDMenu findByUssdSessionStageAndDisplayNumber(USSDSessionStage ussdSessionStage, Long displayNumber);
}
