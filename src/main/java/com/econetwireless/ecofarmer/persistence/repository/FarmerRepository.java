package com.econetwireless.ecofarmer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.econetwireless.ecofarmer.persistence.model.Farmer;

public interface FarmerRepository extends JpaRepository<Farmer, String> {

}
