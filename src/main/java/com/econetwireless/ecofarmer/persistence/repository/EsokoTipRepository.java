package com.econetwireless.ecofarmer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.econetwireless.ecofarmer.persistence.model.EsokoTip;

public interface EsokoTipRepository extends JpaRepository<EsokoTip, Long> {

}
