package com.econetwireless.ecofarmer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.econetwireless.ecofarmer.persistence.model.Deregistration;

public interface DeregistrationRepository extends JpaRepository<Deregistration, Long> {

}
