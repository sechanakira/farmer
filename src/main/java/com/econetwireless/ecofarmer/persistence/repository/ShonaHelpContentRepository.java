package com.econetwireless.ecofarmer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.econetwireless.ecofarmer.persistence.model.ShonaHelpContent;

public interface ShonaHelpContentRepository extends JpaRepository<ShonaHelpContent, Long> {

}
