package com.econetwireless.ecofarmer.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.econetwireless.ecofarmer.persistence.model.USSDSession;

public interface USSDSessionRepository extends JpaRepository<USSDSession, Long> {

	List<USSDSession> findByMsisdn(String msisdn);

	USSDSession findBySessionID(String sessionID);
}
