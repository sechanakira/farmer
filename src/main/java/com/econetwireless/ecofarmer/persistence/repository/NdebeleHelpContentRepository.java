package com.econetwireless.ecofarmer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.econetwireless.ecofarmer.persistence.model.NdebeleHelpContent;

public interface NdebeleHelpContentRepository extends JpaRepository<NdebeleHelpContent, Long> {

}
