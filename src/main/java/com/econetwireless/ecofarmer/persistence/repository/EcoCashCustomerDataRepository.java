package com.econetwireless.ecofarmer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.econetwireless.ecofarmer.persistence.model.EcoCashCustomerData;

public interface EcoCashCustomerDataRepository extends JpaRepository<EcoCashCustomerData, Long> {

	EcoCashCustomerData findByMsisdn(String msisdn);
}
