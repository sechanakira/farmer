package com.econetwireless.ecofarmer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.econetwireless.ecofarmer.persistence.model.RoutingConfig;

public interface RoutingConfigRepository extends JpaRepository<RoutingConfig, Long> {

	RoutingConfig findByApplicationName(String applicationName);
}
