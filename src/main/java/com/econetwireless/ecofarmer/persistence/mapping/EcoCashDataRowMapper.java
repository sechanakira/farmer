package com.econetwireless.ecofarmer.persistence.mapping;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EcocashDataJdbcConstants.ADDRESS1;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EcocashDataJdbcConstants.ADDRESS2;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EcocashDataJdbcConstants.CITY;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EcocashDataJdbcConstants.DOB;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EcocashDataJdbcConstants.FEMALE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EcocashDataJdbcConstants.GENDER;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EcocashDataJdbcConstants.MSISDN;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EcocashDataJdbcConstants.NAT_ID;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.jdbc.core.RowMapper;

import com.econetwireless.ecofarmer.persistence.model.EcoCashCustomerData;
import com.econetwireless.ecofarmer.persistence.model.Gender;

public class EcoCashDataRowMapper implements RowMapper<EcoCashCustomerData> {

	@Override
	public EcoCashCustomerData mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
		EcoCashCustomerData ecoCashCustomerData = new EcoCashCustomerData();
		String gender = resultSet.getString(GENDER);
		if (gender.equals(FEMALE)) {
			ecoCashCustomerData.setGender(Gender.FEMALE);
		} else {
			ecoCashCustomerData.setGender(Gender.MALE);
		}
		ecoCashCustomerData.setAddress1(resultSet.getString(ADDRESS1));
		ecoCashCustomerData.setAddress2(resultSet.getString(ADDRESS2));
		ecoCashCustomerData.setCity(resultSet.getString(CITY));
		String ecocashDOB = resultSet.getString(DOB);
		ecoCashCustomerData.setDateOfBirth(LocalDate.parse(ecocashDOB.substring(0, ecocashDOB.indexOf(' '))));
		ecoCashCustomerData.setMsisdn("263" + resultSet.getString(MSISDN));
		ecoCashCustomerData.setNationalID(resultSet.getString(NAT_ID));
		ecoCashCustomerData.setRegistrationDate(LocalDateTime.now());
		ecoCashCustomerData.setFirstName(resultSet.getString("FIRST_NAME"));
		ecoCashCustomerData.setLastName(resultSet.getString("LAST_NAME"));
		return ecoCashCustomerData;
	}

}
