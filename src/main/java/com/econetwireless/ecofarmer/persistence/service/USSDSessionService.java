package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import com.econetwireless.ecofarmer.persistence.model.USSDSession;

public interface USSDSessionService {

	List<USSDSession> findByMsisdn(String msisdn);

	void saveUSSDSession(USSDSession ussdSession);

	USSDSession findBySessionID(String sessionID);

	void removeExpiredSessions();
}
