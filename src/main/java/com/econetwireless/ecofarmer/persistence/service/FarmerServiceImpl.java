package com.econetwireless.ecofarmer.persistence.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.Deregistration;
import com.econetwireless.ecofarmer.persistence.model.EcoCashCustomerData;
import com.econetwireless.ecofarmer.persistence.model.Farmer;
import com.econetwireless.ecofarmer.persistence.repository.DeregistrationRepository;
import com.econetwireless.ecofarmer.persistence.repository.EcoCashCustomerDataRepository;
import com.econetwireless.ecofarmer.persistence.repository.FarmerRepository;

public class FarmerServiceImpl implements FarmerService {

	@Autowired
	private FarmerRepository farmerRepository;

	@Autowired
	private EcoCashCustomerDataRepository ecoCashCustomerDataRepository;

	@Autowired
	private DeregistrationRepository deregistrationRepository;

	@Override
	public Farmer findByMsisdn(String msisdn) {
		return farmerRepository.findOne(msisdn);
	}

	@Override
	public void saveFarmer(Farmer farmer) {
		farmerRepository.saveAndFlush(farmer);
	}

	@Override
	public EcoCashCustomerData findEcoCashData(String msisdn) {
		return ecoCashCustomerDataRepository.findByMsisdn(msisdn);
	}

	@Override
	public boolean isFullyRegistered(String msisdn) {
		Farmer farmer = findByMsisdn(msisdn);
		EcoCashCustomerData ecoCashCustomerData = ecoCashCustomerDataRepository.findByMsisdn(msisdn);
		if (ecoCashCustomerData == null) {
			return false;
		}
		if(farmer == null){
			return false;
		}
		if (farmer.getProvinceID() == null || farmer.getDistrictID() == null || farmer.getWardNumber() == null || farmer.getFarmTypeID() == null) {
			return false;
		}
		return true;
	}

	@Override
	public void deRegister(String msisdn) {
		Farmer farmer = findByMsisdn(msisdn);
		Deregistration derigistration = new Deregistration();
		derigistration.setDeRegistrationDate(LocalDateTime.now());
		derigistration.setDistrictID(farmer.getDistrictID());
		derigistration.setFarmTypeID(farmer.getFarmTypeID());
		derigistration.setLanguageID(farmer.getLanguageID());
		derigistration.setMsisdn(msisdn);
		derigistration.setProvinceID(farmer.getProvinceID());
		derigistration.setRegistrationDate(farmer.getRegistrationDate());
		derigistration.setWardNumber(farmer.getWardNumber());
		deregistrationRepository.save(derigistration);
		farmerRepository.delete(farmer);
	}

}
