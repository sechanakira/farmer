package com.econetwireless.ecofarmer.persistence.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.repository.USSDSessionRepository;

public class USSDSessionServiceImpl implements USSDSessionService {

	@Autowired
	private USSDSessionRepository ussdSessionRepository;

	@Override
	public List<USSDSession> findByMsisdn(String msisdn) {
		return ussdSessionRepository.findByMsisdn(msisdn);
	}

	@Override
	public void saveUSSDSession(USSDSession ussdSession) {
		ussdSessionRepository.saveAndFlush(ussdSession);
	}

	@Override
	public USSDSession findBySessionID(String sessionID) {
		return ussdSessionRepository.findBySessionID(sessionID);
	}

	@Override
	public void removeExpiredSessions() {
		List<USSDSession> expiredSessions = ussdSessionRepository.findAll().stream().filter(
						ussdSession -> Duration.between(ussdSession.getSessionStartTime(),
										LocalDateTime.now()).getSeconds() >= 5 * 60).collect(Collectors.toList());
		ussdSessionRepository.deleteInBatch(expiredSessions);
	}

}
