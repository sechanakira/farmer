package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import com.econetwireless.ecofarmer.persistence.model.EnglishHelpContent;

public interface EnglishHelpService {

	List<EnglishHelpContent> findAllContent();

	EnglishHelpContent findById(Long id);
}
