package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import com.econetwireless.ecofarmer.persistence.model.EsokoTip;

public interface EsokoTipService {

	List<EsokoTip> findAllEsokoTips();

	EsokoTip findEsokoTip(Long tipId);
}
