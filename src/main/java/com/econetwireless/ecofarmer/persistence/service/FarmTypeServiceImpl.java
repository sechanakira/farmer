package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.FarmType;
import com.econetwireless.ecofarmer.persistence.repository.FarmTypeRepository;

public class FarmTypeServiceImpl implements FarmTypeService {

	@Autowired
	private FarmTypeRepository farmTypeRepository;

	@Override
	public List<FarmType> findAllFarmTypes() {
		return farmTypeRepository.findAll();
	}

}
