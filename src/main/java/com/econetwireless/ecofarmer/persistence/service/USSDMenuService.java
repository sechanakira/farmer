package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import com.econetwireless.ecofarmer.persistence.model.USSDMenu;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;

public interface USSDMenuService {

	List<USSDMenu> findAll();

	List<USSDMenu> findBySessionStage(USSDSessionStage sessionStage);

	USSDMenu findBySessionStageAndDisplayNumber(USSDSessionStage sessionStage, Long displayNumber);
}
