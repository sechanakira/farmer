package com.econetwireless.ecofarmer.persistence.service;

import com.econetwireless.ecofarmer.persistence.model.EsokoAPICallResponse;

@FunctionalInterface
public interface EsokoAPICallResponseService {

	void saveEsokoAPIResponse(EsokoAPICallResponse esokoAPICallResponse);

}
