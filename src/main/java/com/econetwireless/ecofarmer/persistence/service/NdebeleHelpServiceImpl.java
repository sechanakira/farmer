package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.NdebeleHelpContent;
import com.econetwireless.ecofarmer.persistence.repository.NdebeleHelpContentRepository;

public class NdebeleHelpServiceImpl implements NdebeleHelpService {

	@Autowired
	private NdebeleHelpContentRepository ndebeleHelpRepository;

	@Override
	public List<NdebeleHelpContent> findAllContent() {
		return ndebeleHelpRepository.findAll();
	}

	@Override
	public NdebeleHelpContent findById(Long id) {
		return ndebeleHelpRepository.findOne(id);
	}

}
