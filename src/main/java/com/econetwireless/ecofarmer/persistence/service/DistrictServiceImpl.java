package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.District;
import com.econetwireless.ecofarmer.persistence.repository.DistrictRepository;

public class DistrictServiceImpl implements DistrictService {

	@Autowired
	private DistrictRepository districtRepository;

	@Override
	public List<District> findAllDistricts() {
		return districtRepository.findAll();
	}

	@Override
	public List<District> findDistrictsByProvinceID(Long provinceID) {
		return findAllDistricts().stream().filter(district -> district.getProvinceID().equals(provinceID)).collect(
						Collectors.toList());
	}

	@Override
	public District findDistrictByID(Long districtID) {
		return districtRepository.findOne(districtID);
	}

}
