package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.EsokoPaymentMethod;
import com.econetwireless.ecofarmer.persistence.repository.EsokoPaymentMethodRepository;

public class EsokoPaymentMethodServiceImpl implements EsokoPaymentMethodService {
	
	@Autowired
	private EsokoPaymentMethodRepository esokoPaymentMethodRepository;

	@Override
	public List<EsokoPaymentMethod> findAllPaymentMethods() {
		return esokoPaymentMethodRepository.findAll();
	}

	@Override
	public EsokoPaymentMethod findPaymentMethod(Long paymentMethodID) {
		return esokoPaymentMethodRepository.findOne(paymentMethodID);
	}

}
