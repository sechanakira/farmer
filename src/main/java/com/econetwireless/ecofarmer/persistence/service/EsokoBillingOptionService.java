package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import com.econetwireless.ecofarmer.persistence.model.EsokoBillingOption;

public interface EsokoBillingOptionService {

	List<EsokoBillingOption> findAllBillingOptions();

	EsokoBillingOption findBillingOption(Long id);
}
