package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.RoutingConfig;
import com.econetwireless.ecofarmer.persistence.repository.RoutingConfigRepository;

public class RoutingConfigServiceImpl implements RoutingConfigService {

	@Autowired
	private RoutingConfigRepository routingConfigRepository;

	@Override
	public List<RoutingConfig> findAllRoutingConfigs() {
		return routingConfigRepository.findAll();
	}

	@Override
	public RoutingConfig findByApplicationName(String applicationName) {
		return routingConfigRepository.findByApplicationName(applicationName);
	}

}
