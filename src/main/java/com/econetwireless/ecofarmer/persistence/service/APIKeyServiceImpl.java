package com.econetwireless.ecofarmer.persistence.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.APIKey;
import com.econetwireless.ecofarmer.persistence.repository.APIKeyRepository;

public class APIKeyServiceImpl implements APIKeyService {

	@Autowired
	private APIKeyRepository apiKeyRepository;

	@Override
	public APIKey findByApplicationName(String applicationName) {
		return apiKeyRepository.findByApplicationName(applicationName);
	}

}
