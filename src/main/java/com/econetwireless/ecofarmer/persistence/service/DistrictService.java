package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import com.econetwireless.ecofarmer.persistence.model.District;

public interface DistrictService {

	List<District> findAllDistricts();

	List<District> findDistrictsByProvinceID(Long provinceID);

	District findDistrictByID(Long districtID);
}
