package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import com.econetwireless.ecofarmer.persistence.model.ShonaHelpContent;

public interface ShonaHelpService {

	List<ShonaHelpContent> findAllContent();

	ShonaHelpContent findById(Long id);
}
