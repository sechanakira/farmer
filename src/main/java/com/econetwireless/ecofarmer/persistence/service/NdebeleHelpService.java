package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import com.econetwireless.ecofarmer.persistence.model.NdebeleHelpContent;

public interface NdebeleHelpService {

	List<NdebeleHelpContent> findAllContent();

	NdebeleHelpContent findById(Long id);
}
