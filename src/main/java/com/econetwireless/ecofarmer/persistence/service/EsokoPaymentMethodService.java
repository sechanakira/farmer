package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import com.econetwireless.ecofarmer.persistence.model.EsokoPaymentMethod;

public interface EsokoPaymentMethodService {
	
	List<EsokoPaymentMethod> findAllPaymentMethods();
	
	EsokoPaymentMethod findPaymentMethod(Long paymentMethodID);
}
