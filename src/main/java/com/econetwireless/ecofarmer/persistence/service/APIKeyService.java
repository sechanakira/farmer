package com.econetwireless.ecofarmer.persistence.service;

import com.econetwireless.ecofarmer.persistence.model.APIKey;

public interface APIKeyService {

	APIKey findByApplicationName(String applicationName);
}
