package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import com.econetwireless.ecofarmer.persistence.model.RoutingConfig;

public interface RoutingConfigService {

	List<RoutingConfig> findAllRoutingConfigs();

	RoutingConfig findByApplicationName(String applicationName);
}
