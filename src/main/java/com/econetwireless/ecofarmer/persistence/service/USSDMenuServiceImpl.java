package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.USSDMenu;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.repository.USSDMenuRepository;

public class USSDMenuServiceImpl implements USSDMenuService {

	@Autowired
	private USSDMenuRepository ussdMenuRepository;

	@Override
	public List<USSDMenu> findAll() {
		return ussdMenuRepository.findAll();
	}

	@Override
	public List<USSDMenu> findBySessionStage(USSDSessionStage sessionStage) {
		return ussdMenuRepository.findByUssdSessionStage(sessionStage);
	}

	@Override
	public USSDMenu findBySessionStageAndDisplayNumber(USSDSessionStage sessionStage, Long displayNumber) {
		Optional<USSDMenu> ussdMenu = Optional.ofNullable(
						ussdMenuRepository.findByUssdSessionStageAndDisplayNumber(sessionStage, displayNumber));
		return ussdMenu.orElse(findBySessionStage(sessionStage).get(0));
	}

}
