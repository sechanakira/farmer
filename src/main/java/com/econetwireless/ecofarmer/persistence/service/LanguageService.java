package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import com.econetwireless.ecofarmer.persistence.model.Language;

public interface LanguageService {

	List<Language> findAllLanguages();

	Language findByLanguageID(Long languageID);
}
