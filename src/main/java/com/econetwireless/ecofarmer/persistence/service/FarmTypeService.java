package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import com.econetwireless.ecofarmer.persistence.model.FarmType;

public interface FarmTypeService {

	List<FarmType> findAllFarmTypes();

}
