package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.Language;
import com.econetwireless.ecofarmer.persistence.repository.LanguageRepository;

public class LanguageServiceImpl implements LanguageService {

	@Autowired
	private LanguageRepository languageRepository;

	@Override
	public List<Language> findAllLanguages() {
		return languageRepository.findAll();
	}

	@Override
	public Language findByLanguageID(Long languageID) {
		return findAllLanguages().get((int) (languageID - 1));
	}

}
