package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.EsokoBillingOption;
import com.econetwireless.ecofarmer.persistence.repository.EsokoBillingOptionRepository;

public class EsokoBillingOptionServiceImpl implements EsokoBillingOptionService {
	
	@Autowired
	private EsokoBillingOptionRepository esokoBillingOptionRepository;

	@Override
	public List<EsokoBillingOption> findAllBillingOptions() {
		return esokoBillingOptionRepository.findAll();
	}

	@Override
	public EsokoBillingOption findBillingOption(Long id) {
		return esokoBillingOptionRepository.findOne(id);
	}

}
