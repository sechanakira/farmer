package com.econetwireless.ecofarmer.persistence.service;

import com.econetwireless.ecofarmer.persistence.model.EcoCashCustomerData;
import com.econetwireless.ecofarmer.persistence.model.Farmer;

public interface FarmerService {

	Farmer findByMsisdn(String msisdn);

	void saveFarmer(Farmer farmer);

	EcoCashCustomerData findEcoCashData(String msisdn);

	boolean isFullyRegistered(String msisdn);

	void deRegister(String msisdn);
}
