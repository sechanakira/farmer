package com.econetwireless.ecofarmer.persistence.service;

import com.econetwireless.ecofarmer.persistence.model.EcoCashCustomerData;

public interface EcoCashCustomerDataService {

	EcoCashCustomerData findByMSISDN(String msisdn);

	EcoCashCustomerData findCustomerInEcoCash(String msisdn);

	void findAndSaveEcoCashData(String msisdn);

}
