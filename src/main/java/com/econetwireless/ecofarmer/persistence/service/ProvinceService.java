package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import com.econetwireless.ecofarmer.persistence.model.Province;

public interface ProvinceService {

	List<Province> findAllProvinces();

	Province findProvinceByID(Long provinceID);

}
