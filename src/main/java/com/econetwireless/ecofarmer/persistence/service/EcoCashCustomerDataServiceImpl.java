package com.econetwireless.ecofarmer.persistence.service;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EcocashDataJdbcConstants.SQL_SELECT_QUERY;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.econetwireless.ecofarmer.persistence.model.EcoCashCustomerData;
import com.econetwireless.ecofarmer.persistence.repository.EcoCashCustomerDataRepository;

public class EcoCashCustomerDataServiceImpl implements EcoCashCustomerDataService {

	@Autowired
	private EcoCashCustomerDataRepository ecoCashCustomerDataRepository;

	@Autowired
	private JdbcTemplate ecocashDS;

	private RowMapper<EcoCashCustomerData> rowMapper;

	public EcoCashCustomerDataServiceImpl(RowMapper<EcoCashCustomerData> rowMapper) {
		this.rowMapper = rowMapper;
	}

	@Override
	public EcoCashCustomerData findByMSISDN(String msisdn) {
		return ecoCashCustomerDataRepository.findByMsisdn(msisdn);
	}

	@Override
	public EcoCashCustomerData findCustomerInEcoCash(String msisdn) {
		try {
			return ecocashDS.queryForObject(SQL_SELECT_QUERY, new Object[] { msisdn.substring(3, msisdn.length()) },
							new int[] { 1 }, rowMapper);
		} catch (Exception ex) {
			return null;
		}
	}

	@Override
	public void findAndSaveEcoCashData(String msisdn) {
		EcoCashCustomerData ecoCashCustomerData = findCustomerInEcoCash(msisdn);
		ecoCashCustomerDataRepository.saveAndFlush(ecoCashCustomerData);
	}

}
