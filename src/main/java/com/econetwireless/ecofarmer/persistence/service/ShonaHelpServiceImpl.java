package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.ShonaHelpContent;
import com.econetwireless.ecofarmer.persistence.repository.ShonaHelpContentRepository;

public class ShonaHelpServiceImpl implements ShonaHelpService {

	@Autowired
	private ShonaHelpContentRepository shonaHelpRepository;

	@Override
	public List<ShonaHelpContent> findAllContent() {
		return shonaHelpRepository.findAll();
	}

	@Override
	public ShonaHelpContent findById(Long id) {
		return shonaHelpRepository.findOne(id);
	}

}
