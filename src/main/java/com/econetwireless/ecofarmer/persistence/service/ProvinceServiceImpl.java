package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.Province;
import com.econetwireless.ecofarmer.persistence.repository.ProvinceRepository;

public class ProvinceServiceImpl implements ProvinceService {

	@Autowired
	private ProvinceRepository provinceRepository;

	@Override
	public List<Province> findAllProvinces() {
		return provinceRepository.findAll();
	}

	@Override
	public Province findProvinceByID(Long provinceID) {
		return findAllProvinces().get((int) (provinceID - 1));
	}

}
