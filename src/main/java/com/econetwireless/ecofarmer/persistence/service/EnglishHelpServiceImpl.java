package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.EnglishHelpContent;
import com.econetwireless.ecofarmer.persistence.repository.EnglishHelpContentRepository;

public class EnglishHelpServiceImpl implements EnglishHelpService {

	@Autowired
	private EnglishHelpContentRepository englishRepository;

	@Override
	public List<EnglishHelpContent> findAllContent() {
		return englishRepository.findAll();
	}

	@Override
	public EnglishHelpContent findById(Long id) {
		return englishRepository.findOne(id);
	}

}
