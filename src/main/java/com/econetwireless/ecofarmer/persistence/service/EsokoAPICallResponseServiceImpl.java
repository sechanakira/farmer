package com.econetwireless.ecofarmer.persistence.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.EsokoAPICallResponse;
import com.econetwireless.ecofarmer.persistence.repository.EsokoAPICallResponseRepository;

public class EsokoAPICallResponseServiceImpl implements EsokoAPICallResponseService {

	@Autowired
	private EsokoAPICallResponseRepository esokoAPICallResponseRepository;

	@Override
	public void saveEsokoAPIResponse(EsokoAPICallResponse esokoAPICallResponse) {
		esokoAPICallResponseRepository.save(esokoAPICallResponse);
	}

}
