package com.econetwireless.ecofarmer.persistence.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.econetwireless.ecofarmer.persistence.model.EsokoTip;
import com.econetwireless.ecofarmer.persistence.repository.EsokoTipRepository;

public class EsokoTipServiceImpl implements EsokoTipService{
	
	@Autowired
	private EsokoTipRepository esokoTipRepository;

	@Override
	public List<EsokoTip> findAllEsokoTips() {
		return esokoTipRepository.findAll();
	}

	@Override
	public EsokoTip findEsokoTip(Long tipId) {
		return esokoTipRepository.findOne(tipId);
	}

}
