package com.econetwireless.ecofarmer.sms;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Sms implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1545353535353L;

	private Long id;
	private long version;
	private String sourceAddress;
	private String destinationNumber;
	private String notificationUrl;
	private String submitId;
	private String status;
	private Date timeReceived;
	private Date timeRouted;
	private Date deliveryTime;
	private String message;
	private String sender;
	private String clientCorrelator;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getSourceAddress() {
		return sourceAddress;
	}

	public void setSourceAddress(String sourceAddress) {
		this.sourceAddress = sourceAddress;
	}

	public String getDestinationNumber() {
		return destinationNumber;
	}

	public void setDestinationNumber(String destinationNumber) {
		this.destinationNumber = destinationNumber;
	}

	public String getNotificationUrl() {
		return notificationUrl;
	}

	public void setNotificationUrl(String notificationUrl) {
		this.notificationUrl = notificationUrl;
	}

	public String getSubmitId() {
		return submitId;
	}

	public void setSubmitId(String submitId) {
		this.submitId = submitId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTimeReceived() {
		return timeReceived;
	}

	public void setTimeReceived(Date timeReceived) {
		this.timeReceived = timeReceived;
	}

	public Date getTimeRouted() {
		return timeRouted;
	}

	public void setTimeRouted(Date timeRouted) {
		this.timeRouted = timeRouted;
	}

	public Date getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(Date deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getClientCorrelator() {
		return clientCorrelator;
	}

	public void setClientCorrelator(String clientCorrelator) {
		this.clientCorrelator = clientCorrelator;
	}

	@Override
	public String toString() {
		return "Sms [id=" + id + ", version=" + version + ", sourceAddress=" + sourceAddress + ", destinationNumber=" + destinationNumber + ", notificationUrl=" + notificationUrl + ", submitId=" + submitId + ", status=" + status + ", timeReceived=" + timeReceived + ", timeRouted=" + timeRouted + ", deliveryTime=" + deliveryTime + ", message=" + message + ", sender=" + sender + ", clientCorrelator=" + clientCorrelator + "]";
	}

}
