package com.econetwireless.ecofarmer.sms;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.SMSConstants.SMS_NOTIFICATION_URL;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.SMSConstants.SMS_SERVER_URL;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.SMSConstants.SMS_SOURCE_ADDRESS;

import org.springframework.scheduling.annotation.Async;
import org.springframework.web.client.RestTemplate;

public class SMSBeanImpl implements SMSBean {

	private RestTemplate restTemplate;

	public SMSBeanImpl(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Async
	@Override
	public void sendSMS(String msisdn, String msg) {
		Sms sms = new Sms();
		sms.setClientCorrelator("GN" + System.currentTimeMillis() + "");
		sms.setDestinationNumber(msisdn);
		sms.setSourceAddress(SMS_SOURCE_ADDRESS);
		sms.setMessage(msg);
		sms.setNotificationUrl(SMS_NOTIFICATION_URL);
		restTemplate.postForEntity(SMS_SERVER_URL, sms, Sms.class);
	}

}
