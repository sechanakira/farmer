package com.econetwireless.ecofarmer.sms;

@FunctionalInterface
public interface SMSBean {

	public void sendSMS(String msisdn, String msg);

}
