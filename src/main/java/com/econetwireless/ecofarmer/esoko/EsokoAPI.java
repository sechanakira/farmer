package com.econetwireless.ecofarmer.esoko;

public interface EsokoAPI {

	EsokoResponse subscribeRequest(EsokoRequest esokoRequest);

	EsokoResponse unsubscribeRequest(EsokoRequest esokoRequest);

	EsokoResponse changeSubscription(EsokoRequest esokoRequest);

	EsokoResponse changeLanguage(EsokoRequest esokoRequest);

	EsokoResponse subscribeToFarmingClub(EsokoRequest esokoRequest);

	EsokoResponse offerToSell(EsokoRequest esokoRequest);

	EsokoResponse editSubscription(EsokoRequest esokoRequest);

	EsokoResponse changeBilling(EsokoRequest esokoRequest);

	EsokoResponse unsubscribe(EsokoRequest esokoRequest);
}
