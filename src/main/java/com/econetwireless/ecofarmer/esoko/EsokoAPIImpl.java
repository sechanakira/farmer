package com.econetwireless.ecofarmer.esoko;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.API_KEY;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.BILLING_CYCLE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.COMMODITY_CODE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.ESOKO_APPLICATION_NAME;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.ESOKO_OFFER_URL;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.ESOKO_URL;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.ESOKO_URL_CHANGE_SUB;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.ESOKO_URL_UNSUB;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.FORMAT;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.LANGUAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.LOCATION_CODE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.MEASURE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.PHONE_NUMBER;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.PRICE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.QUANTITY;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.REFERENCE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.REFERENCE_PREFIX;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.REPLACE_TIP_TYPE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.TIP_TYPE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EsokoParamNames.XML;

import java.util.Random;

import org.springframework.scheduling.annotation.Async;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.econetwireless.ecofarmer.persistence.model.EsokoAPICallResponse;
import com.econetwireless.ecofarmer.persistence.service.APIKeyService;
import com.econetwireless.ecofarmer.persistence.service.EsokoAPICallResponseService;

public class EsokoAPIImpl implements EsokoAPI {

	private RestTemplate restTemplate;

	private APIKeyService apiKeyService;

	private EsokoAPICallResponseService esokoAPICallResponseService;

	private final MultiValueMap<String, String> formValuesMap = new LinkedMultiValueMap<>();

	@Override
	public EsokoResponse subscribeRequest(EsokoRequest esokoRequest) {
		formValuesMap.add(PHONE_NUMBER, esokoRequest.getMsisdn());
		formValuesMap.add(LANGUAGE, esokoRequest.getLanguage());
		formValuesMap.add(BILLING_CYCLE, esokoRequest.getBillingCycle());
		formValuesMap.add(TIP_TYPE, esokoRequest.getTipType());
		return buildAndPostForm(ESOKO_URL);
	}

	@Override
	public EsokoResponse unsubscribeRequest(EsokoRequest esokoRequest) {
		formValuesMap.add(PHONE_NUMBER, esokoRequest.getMsisdn());
		formValuesMap.add(TIP_TYPE, esokoRequest.getTipType());
		return buildAndPostForm(ESOKO_URL_UNSUB);
	}

	@Override
	public EsokoResponse changeSubscription(EsokoRequest esokoRequest) {
		formValuesMap.add(PHONE_NUMBER, esokoRequest.getMsisdn());
		formValuesMap.add(BILLING_CYCLE, esokoRequest.getBillingCycle());
		return buildAndPostForm(ESOKO_URL_CHANGE_SUB);
	}

	@Override
	public EsokoResponse changeLanguage(EsokoRequest esokoRequest) {
		formValuesMap.add(PHONE_NUMBER, esokoRequest.getMsisdn());
		formValuesMap.add(LANGUAGE, esokoRequest.getLanguage());
		return buildAndPostForm(ESOKO_URL_CHANGE_SUB);
	}

	@Override
	public EsokoResponse subscribeToFarmingClub(EsokoRequest esokoRequest) {
		formValuesMap.add(LOCATION_CODE, esokoRequest.getLocationCode());
		formValuesMap.add(PHONE_NUMBER, esokoRequest.getMsisdn());
		formValuesMap.add(LANGUAGE, esokoRequest.getLanguage());
		formValuesMap.add(BILLING_CYCLE, esokoRequest.getBillingCycle());
		formValuesMap.add(TIP_TYPE, esokoRequest.getTipType());
		return buildAndPostForm(ESOKO_URL);
	}

	@Override
	public EsokoResponse offerToSell(EsokoRequest esokoRequest) {
		formValuesMap.add(MEASURE, esokoRequest.getMeasure());
		formValuesMap.add(PRICE, esokoRequest.getPrice());
		formValuesMap.add(QUANTITY, esokoRequest.getQuantity());
		formValuesMap.add(PHONE_NUMBER, esokoRequest.getMsisdn());
		formValuesMap.add(LANGUAGE, esokoRequest.getLanguage());
		formValuesMap.add(BILLING_CYCLE, esokoRequest.getBillingCycle());
		formValuesMap.add(COMMODITY_CODE, esokoRequest.getCommodityCode());
		return buildAndPostForm(ESOKO_OFFER_URL);
	}

	@Override
	public EsokoResponse editSubscription(EsokoRequest esokoRequest) {
		formValuesMap.add(PHONE_NUMBER, esokoRequest.getMsisdn());
		formValuesMap.add(TIP_TYPE, esokoRequest.getTipType());
		formValuesMap.add(REPLACE_TIP_TYPE, esokoRequest.getReplaceTipType());
		return buildAndPostForm(ESOKO_URL_CHANGE_SUB);
	}

	@Override
	public EsokoResponse changeBilling(EsokoRequest esokoRequest) {
		formValuesMap.add(PHONE_NUMBER, esokoRequest.getMsisdn());
		formValuesMap.add(BILLING_CYCLE, esokoRequest.getBillingCycle());
		formValuesMap.add(TIP_TYPE, esokoRequest.getTipType());
		return buildAndPostForm(ESOKO_URL_CHANGE_SUB);
	}

	@Override
	public EsokoResponse unsubscribe(EsokoRequest esokoRequest) {
		formValuesMap.add(PHONE_NUMBER, esokoRequest.getMsisdn());
		formValuesMap.add(TIP_TYPE, esokoRequest.getTipType());
		return buildAndPostForm(ESOKO_URL_UNSUB);
	}

	private String generateReference() {
		return REFERENCE_PREFIX + System.currentTimeMillis() + (new Random().nextInt(100));
	}

	private EsokoResponse buildAndPostForm(String url) {
		formValuesMap.add(FORMAT, XML);
		formValuesMap.add(REFERENCE, generateReference());
		formValuesMap.add(API_KEY, apiKeyService.findByApplicationName(ESOKO_APPLICATION_NAME).getApiKey());
		EsokoResponse esokoResponse = restTemplate.postForObject(url, formValuesMap, EsokoResponse.class);
		saveEsokoResponse(esokoResponse, formValuesMap.getFirst(PHONE_NUMBER), formValuesMap.getFirst(REFERENCE));
		return esokoResponse;
	}

	@Async
	private void saveEsokoResponse(EsokoResponse esokoResponse, String msisdn, String reference) {
		EsokoAPICallResponse esokoAPICallResponse = new EsokoAPICallResponse();
		esokoAPICallResponse.setDescription(esokoResponse.getDescription());
		esokoAPICallResponse.setMsisdn(msisdn);
		esokoAPICallResponse.setReference(reference);
		esokoAPICallResponse.setStatus(esokoResponse.getStatus());
		esokoAPICallResponseService.saveEsokoAPIResponse(esokoAPICallResponse);
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public void setApiKeyService(APIKeyService apiKeyService) {
		this.apiKeyService = apiKeyService;
	}

	public void setEsokoAPICallResponseService(EsokoAPICallResponseService esokoAPICallResponseService) {
		this.esokoAPICallResponseService = esokoAPICallResponseService;
	}

}
