package com.econetwireless.ecofarmer.esoko;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EsokoRequest {

	private String msisdn;
	private String language;
	private String billingCycle;
	private String tipType;
	private String locationCode;
	private String measure;
	private String price;
	private String quantity;
	private String commodityCode;
	private String replaceTipType;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getBillingCycle() {
		return billingCycle;
	}

	public void setBillingCycle(String billingCycle) {
		this.billingCycle = billingCycle;
	}

	public String getTipType() {
		return tipType;
	}

	public void setTipType(String tipType) {
		this.tipType = tipType;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public String getMeasure() {
		return measure;
	}

	public void setMeasure(String measure) {
		this.measure = measure;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getCommodityCode() {
		return commodityCode;
	}

	public void setCommodityCode(String commodityCode) {
		this.commodityCode = commodityCode;
	}

	public String getReplaceTipType() {
		return replaceTipType;
	}

	public void setReplaceTipType(String replaceTipType) {
		this.replaceTipType = replaceTipType;
	}

}
