package com.econetwireless.ecofarmer.esoko;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "esoko")
@XmlAccessorType(XmlAccessType.FIELD)
public class EsokoResponse {

	@XmlElement
	private String status;
	@XmlElement
	private String description;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "EsokoResponse{" + "status='" + status + '\'' + ", description='" + description + '\'' + '}';
	}
}
