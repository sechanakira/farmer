package com.econetwireless.ecofarmer.constants;

public class ApplicationConstants {

	private ApplicationConstants() {
	}

	public class SMSConstants {

		public static final String SMS_SOURCE_ADDRESS = "263144";
		public static final String SMS_NOTIFICATION_URL = "";
		public static final String SMS_SERVER_URL = "http://sms-gateway:8080/smsgateway/rest/sms/send";

		private SMSConstants() {
		}

	}

	public class EmailConstants {

		public static final String EMAIL_SOURCE_ADDRESS = "sysdev@econet.co.zw";
		public static final String SMTP_HOST = "192.168.101.60";

		private EmailConstants() {
		}
	}

	public class GatewayConstants {

		public static final String GATEWAY_STAGE_FIRST = "FIRST";
		public static final String GATEWAY_STAGE_COMPLETE = "COMPLETE";
		public static final String GATEWAY_STAGE_PENDING = "PENDING";
		public static final String USSD_CHANNEL = "USSD";
		public static final String TRANSACTION_ID = "TRANSACTION_ID";
		public static final String TRANSACTION_TYPE = "MENU_PROCESSING";
		public static final String INVALID_OPTION_MESSAGE = "You have selected an invalid option";
		public static final String THANK_YOU_MESSAGE = "Thanks for registering for EcoFarmer, dial *144# to enjoy EcoFarmer services";
		public static final String CANCEL_MESSAGE = "Thanks for using EcoFarmer";
		public static final String SESSION_EXPIRED = "Your session has expired, please redial short code to access the service";
		public static final String UNAVAILABLE_OPTION = "This option is currently not available";

		private GatewayConstants() {
		}
	}

	public class USSDMenuHeaderConstants {

		public static final String WELCOME_HEADER = "Welcome to EcoFarmer \n";
		public static final String ECOFARMER_HEADER = "EcoFarmer \n";

		private USSDMenuHeaderConstants() {
		}
	}

	public class EcocashDataJdbcConstants {

		public static final String SQL_SELECT_QUERY = "SELECT * FROM ECOFARMER.MTX_PARTY_VIEW WHERE msisdn = ? AND status = 'Y'";
		public static final String ADDRESS1 = "ADDRESS1";
		public static final String ADDRESS2 = "ADDRESS2";
		public static final String CITY = "CITY";
		public static final String DOB = "DOB";
		public static final String FIRST_NAME = "FIRST_NAME";
		public static final String GENDER = "GENDER";
		public static final String LAST_NAME = "LAST_NAME";
		public static final String MSISDN = "MSISDN";
		public static final String NAT_ID = "NAT_ID";
		public static final String STATUS = "STATUS";
		public static final String MALE = "M";
		public static final String FEMALE = "F";

		private EcocashDataJdbcConstants() {
		}
	}

	public class USSDSelectionConstants {

		public static final String ACCEPT_OPTION = "1";
		public static final String CANCEL_OPTION = "2";

		private USSDSelectionConstants() {
		}
	}

	public class NationalIDConstants {

		public static final int FIRST_ID_DIGIT = 0;
		public static final int SECOND_ID_DIGIT = 1;
		public static final int THIRD_ID_DIGIT = 2;
		public static final int FOURTH_ID_DIGIT = 3;
		public static final int FIFTH_ID_DIGIT = 4;

		private NationalIDConstants() {
		}
	}

	public class DistrictSelectionConstants {

		public static final String NEXT_MENU_LIST_SELECTION = "0";
		public static final int MAX_MENU_ITEMS = 9;

		private DistrictSelectionConstants() {
		}
	}

	public class USSDRoutingConstants {

		public static final String METHOD_NAME_HANDLE_REQUEST = "handleUSSDRequest";
		public static final String USSD_RESPONSE_STRING = "USSDResponseString";
		public static final String USSD_ACTION = "action";
		public static final String USSD_END = "end";
		public static final String SDP_TRANSACTION_ID = "TransactionId";
		public static final String SDP_TRANSACTION_TIME = "TransactionTime";
		public static final String SDP_MSISDN = "MSISDN";
		public static final String SDP_USSD_SERVICE_CODE = "USSDServiceCode";
		public static final String SDP_USSD_REQUEST_STRING = "USSDRequestString";
		public static final String SDP_RESPONSE = "response";
		public static final String SDP_SEQUENCE = "Sequence";
		public static final String SDP_RESPONSE_FALSE = "false";
		public static final String SDP_IMSI = "IMSI";
		public static final String SDP_EMPTY_STRING = "";
		public static final String SDP_SESSION_ID = "SessionId";
		public static final String SDP_VLR = "VLR";
		public static final String FIRST_REQUEST_MESSAGE = "#";
		public static final String FIRST_REQUEST_STAGE = "FIRST";
		public static final String ECOFARMER_SHORT_CODE = "144";
		public static final String SDP_FIRST_REQUEST_MESSAGE = "*3#";

		private USSDRoutingConstants() {
		}
	}

	public class ApplicationNames {

		public static final String ECOFARMER_TIPS = "ECOFARMER_TIPS";
		public static final String ECOSURE = "ECOSURE";
		public static final String BIDS_AND_OFFER = "BIDS_AND_OFFER";

		private ApplicationNames() {
		}
	}

	public class ZFUConstants {

		public static final String ECOSURE_BASE_URL = "http://192.168.101.63:8080/ecosure-web/jaxrs/";
		public static final String ECOSURE_AUTH_URL = ECOSURE_BASE_URL + "authenticate";
		public static final String ECOSURE_REG_URL = ECOSURE_BASE_URL + "isRegistered";
		public static final String ECOSURE_PRODUCTS_URL = ECOSURE_BASE_URL + "getPackages";
		public static final String ECOSURE_REGISTER = ECOSURE_BASE_URL + "register";

		private ZFUConstants() {
		}
	}

	public class EsokoConstants {

		public static final String API_KEY = "api_key";
		public static final String PHONE_NUMBER = "phone_number";
		public static final String TIP_TYPE = "tip_type";
		public static final String REFERENCE = "reference";
		public static final String FORMAT = "format";
		public static final String BILLING_CYCLE = "billing_cycle";

		private EsokoConstants() {
		}
	}

	public static class EsokoParamNames {

		public static final String MEASURE = "measure";
		public static final String COMMODITY_CODE = "commodity_code";
		public static final String REPLACE_TIP_TYPE = "replace_tip_type";
		public static final String PRICE = "price";
		public static final String QUANTITY = "quantity";
		public static final String LOCATION_CODE = "location_code";
		public static final String API_KEY = "api_key";
		public static final String PHONE_NUMBER = "phone_number";
		public static final String LANGUAGE = "language";
		public static final String BILLING_CYCLE = "billing_cycle";
		public static final String TIP_TYPE = "tip_type";
		public static final String REFERENCE = "reference";
		public static final String FORMAT = "format";
		public static final String XML = "xml";
		public static final String JSON = "json";

		public static final String ESOKO_URL = "http://172.16.0.4/api/custom/v1/subscribe/ecofarmer/";
		public static final String ESOKO_URL_UNSUB = "http://172.16.0.4/api/custom/v1/unsubscribe/ecofarmer/";
		public static final String ESOKO_URL_CHANGE_SUB = "http://172.16.0.4/api/custom/v1/editsubscribe/ecofarmer/";
		public static final String ECOLIFE_URL = "http://172.16.0.4/ecolifeussd/USSDReceiver";
		public static final String ESOKO_OFFER_URL = "https://172.16.0.4/api/custom/v1/offer/ecofarmer/";

		public static final String ESOKO_API_KEY = "ESOKO_API_KEY";
		public static final String ESOKO_APPLICATION_NAME = "ESOKO";
		public static final String REFERENCE_PREFIX = "REF";

		private EsokoParamNames() {
		}
	}

}
