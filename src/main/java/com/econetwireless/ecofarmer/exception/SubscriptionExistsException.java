package com.econetwireless.ecofarmer.exception;

public class SubscriptionExistsException extends Exception {

	private static final long serialVersionUID = -7834042166648612291L;

	public SubscriptionExistsException(String message) {
		super(message);
	}
}
