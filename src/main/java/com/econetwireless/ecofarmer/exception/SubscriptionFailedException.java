package com.econetwireless.ecofarmer.exception;

public class SubscriptionFailedException extends Exception {

	private static final long serialVersionUID = 1580902823356989986L;

	public SubscriptionFailedException(String message) {
		super(message);
	}

}
