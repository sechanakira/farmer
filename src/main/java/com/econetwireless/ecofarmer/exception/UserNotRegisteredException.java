package com.econetwireless.ecofarmer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Shingirai.Chanakira on 2/19/2017.
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="User Not Found")
public class UserNotRegisteredException extends RuntimeException {

    public UserNotRegisteredException(){
        super("User Not Registered");
    }
}
