package com.econetwireless.ecofarmer.inteceptor.logging;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;

@Aspect
@Component
public class LoggingAspect {

	private static final Logger logger = Logger.getLogger(LoggingAspect.class);

	/*
	@Before("execution(* processUSSDRequest(*))")
	public void logUSSDRequest(final JoinPoint joinPoint) {
		Arrays.stream(joinPoint.getArgs()).filter(arg -> arg instanceof MessageRequestType).forEach(
						ussdRequest -> logger.error("Received USSD Request: " + ussdRequest));
	}

	@After("execution(* processUSSDRequest(*))")
	public void logUSSDResponse(final JoinPoint joinPoint) {
		Arrays.stream(joinPoint.getArgs()).filter(arg -> arg instanceof MessageResponseType).forEach(
						ussdResponse -> logger.error("Sending USSD Response: " + ussdResponse));
	}

	@AfterThrowing(pointcut = "execution(* *)", throwing = "exception")
	public void logException(final Exception exception) {
		logger.error("Exception", exception);
	}
	*/
}
