package com.econetwireless.ecofarmer.ussd.ecofarmerservices;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.ApplicationNames.BIDS_AND_OFFER;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.ApplicationNames.ECOSURE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_PENDING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDMenuHeaderConstants.ECOFARMER_HEADER;
import static com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler.createMessageResponse;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNumeric;

import java.util.Optional;

import com.econetwireless.ecofarmer.persistence.model.EcoFarmerServicesSessionData;
import com.econetwireless.ecofarmer.persistence.model.RoutingSessionData;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.routing.USSDRoutingBean;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;

public class EcoFarmerServicesHandler implements AbstractUSSDMenuHandler {

	private USSDSessionService ussdSessionService;

	private USSDRoutingBean ussdRoutingBean;

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		ussdSession.setNextStage(USSDSessionStage.ECOFARMER_SERVICES);
		Optional<EcoFarmerServicesSessionData> sessionData = Optional.ofNullable(
						ussdSession.getEcoFarmerServicesSessionData());
		if (sessionData.isPresent()) {
			if (sessionData.get().getStage().equals(USSDSessionStage.ECOFARMER_SERVICES) && isValidResponseMessage(
							request.getMessage().trim())) {
				Long selectedOption = Long.parseLong(request.getMessage().trim());
				if (selectedOption == 1) {
					RoutingSessionData ussdRoutingSessionData = new RoutingSessionData();
					ussdRoutingSessionData.setApplicationName(BIDS_AND_OFFER);
					ussdRoutingSessionData.setCurrentSequencePosition(0L);
					EcoFarmerServicesSessionData subscriberData = sessionData.get();
					subscriberData.setStage(USSDSessionStage.BIDS_AND_OFFER);
					ussdSession.setRoutingSessionData(ussdRoutingSessionData);
					ussdSessionService.saveUSSDSession(ussdSession);
					return ussdRoutingBean.routeUSSDMessage(request);
				}
				if (selectedOption == 2) {
					RoutingSessionData ussdRoutingSessionData = new RoutingSessionData();
					ussdRoutingSessionData.setApplicationName(ECOSURE);
					ussdRoutingSessionData.setCurrentSequencePosition(0L);
					EcoFarmerServicesSessionData subscriberData = sessionData.get();
					subscriberData.setStage(USSDSessionStage.ECOSURE);
					ussdSession.setRoutingSessionData(ussdRoutingSessionData);
					ussdSessionService.saveUSSDSession(ussdSession);
					return ussdRoutingBean.routeUSSDMessage(request);
				}
			}
			if (sessionData.get().getStage().equals(USSDSessionStage.BIDS_AND_OFFER)) {
				return ussdRoutingBean.routeUSSDMessage(request);
			}
			if (sessionData.get().getStage().equals(USSDSessionStage.ECOSURE)) {
				return ussdRoutingBean.routeUSSDMessage(request);
			}
		} else {
			EcoFarmerServicesSessionData subscriberSessionData = new EcoFarmerServicesSessionData();
			subscriberSessionData.setStage(USSDSessionStage.ECOFARMER_SERVICES);
			ussdSession.setEcoFarmerServicesSessionData(subscriberSessionData);
			ussdSessionService.saveUSSDSession(ussdSession);
			String subscriberMessage = ECOFARMER_HEADER + "Select service \n1.Bids and Offer \n2.EcoSure";
			return createMessageResponse(request, subscriberMessage, GATEWAY_STAGE_PENDING);
		}
		return null;
	}

	private boolean isValidResponseMessage(String responseMessage) {
		if (isBlank(responseMessage)) {
			return false;
		}
		if (!isNumeric(responseMessage)) {
			return false;
		}
		Long selectedOption = Long.parseLong(responseMessage);
		if (selectedOption <= 0 || selectedOption > 2) {
			return false;
		}
		return true;
	}

	public void setUssdSessionService(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	public void setUssdRoutingBean(USSDRoutingBean ussdRoutingBean) {
		this.ussdRoutingBean = ussdRoutingBean;
	}

}
