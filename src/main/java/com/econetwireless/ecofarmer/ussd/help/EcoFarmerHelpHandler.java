package com.econetwireless.ecofarmer.ussd.help;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.CANCEL_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_PENDING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.INVALID_OPTION_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDMenuHeaderConstants.ECOFARMER_HEADER;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDSelectionConstants.ACCEPT_OPTION;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDSelectionConstants.CANCEL_OPTION;
import static com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler.createMessageResponse;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNumeric;

import java.util.Optional;

import com.econetwireless.ecofarmer.persistence.model.HelpSessionData;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.service.EnglishHelpService;
import com.econetwireless.ecofarmer.persistence.service.FarmerService;
import com.econetwireless.ecofarmer.persistence.service.NdebeleHelpService;
import com.econetwireless.ecofarmer.persistence.service.ShonaHelpService;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;

public class EcoFarmerHelpHandler implements AbstractUSSDMenuHandler {

	private FarmerService farmerService;

	private ShonaHelpService shonaHelpService;

	private NdebeleHelpService ndebeleHelpService;

	private EnglishHelpService englishHelpService;

	private USSDSessionService ussdSessionService;

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		ussdSession.setNextStage(USSDSessionStage.HELP);
		Optional<HelpSessionData> sessionData = Optional.ofNullable(ussdSession.getHelpSessionData());
		if (sessionData.isPresent()) {
			if (sessionData.get().getStage().equals(USSDSessionStage.HELP_ANSWER) && isValidResponse(
							request.getMessage().trim())) {
				if (Long.parseLong(request.getMessage().trim()) == 5) {
					HelpSessionData subscriberSessionData = new HelpSessionData();
					subscriberSessionData.setStage(USSDSessionStage.DEREGISTER);
					ussdSession.setHelpSessionData(subscriberSessionData);
					ussdSessionService.saveUSSDSession(ussdSession);
					return createMessageResponse(request,
									ECOFARMER_HEADER + "Confirm EcoFarmer deregistration \n1.Confirm \n2.Cancel",
									GATEWAY_STAGE_PENDING);
				} else {
					return createMessageResponse(request, ECOFARMER_HEADER + createHelpResponse(ussdSession.getMsisdn(),
									request.getMessage().trim()), GATEWAY_STAGE_COMPLETE);
				}
			} else if (sessionData.get().getStage().equals(USSDSessionStage.DEREGISTER) && isValidConfirmationResponse(
							request.getMessage().trim())) {
				if (ACCEPT_OPTION.equals(request.getMessage().trim())) {
					farmerService.deRegister(ussdSession.getMsisdn());
					return createMessageResponse(request,
									"You have successfully deregistered from EcoFarmer. Thank you for using Econet",
									GATEWAY_STAGE_COMPLETE);
				} else if (CANCEL_OPTION.equals(request.getMessage().trim())) {
					return createMessageResponse(request, CANCEL_MESSAGE, GATEWAY_STAGE_COMPLETE);
				} else {
					return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
				}
			} else {
				return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
			}
		} else {
			HelpSessionData subscriberSessionData = new HelpSessionData();
			subscriberSessionData.setStage(USSDSessionStage.HELP_ANSWER);
			ussdSession.setHelpSessionData(subscriberSessionData);
			ussdSessionService.saveUSSDSession(ussdSession);
			return createMessageResponse(request, ECOFARMER_HEADER + createHelpMessage(ussdSession.getMsisdn()),
							GATEWAY_STAGE_PENDING);
		}
	}

	public void setEnglishHelpService(EnglishHelpService englishHelpService) {
		this.englishHelpService = englishHelpService;
	}

	public void setFarmerService(FarmerService farmerService) {
		this.farmerService = farmerService;
	}

	public void setShonaHelpService(ShonaHelpService shonaHelpService) {
		this.shonaHelpService = shonaHelpService;
	}

	public void setNdebeleHelpService(NdebeleHelpService ndebeleHelpService) {
		this.ndebeleHelpService = ndebeleHelpService;
	}

	public void setUssdSessionService(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	private String createHelpMessage(String msisdn) {
		Long subscriberLanguageID = getSubscriberLanguageCode(msisdn);
		if (subscriberLanguageID == 1) {
			return shonaHelpService.findAllContent().stream().map(
							helpContent -> helpContent.getId() + "." + helpContent.getHelpQuestion()).collect(
											joining("\n"));
		} else if (subscriberLanguageID == 2) {
			return ndebeleHelpService.findAllContent().stream().map(
							helpContent -> helpContent.getId() + "." + helpContent.getHelpQuestion()).collect(
											joining("\n"));
		} else {
			return englishHelpService.findAllContent().stream().map(
							helpContent -> helpContent.getId() + "." + helpContent.getHelpQuestion()).collect(
											joining("\n"));
		}
	}

	private String createHelpResponse(String msisdn, String responseMessage) {
		Long subscriberLanguageID = getSubscriberLanguageCode(msisdn);
		Long subscriberSelection = Long.parseLong(responseMessage);
		if (subscriberLanguageID == 1) {
			return shonaHelpService.findById(subscriberSelection).getHelpAnswer();
		} else if (subscriberLanguageID == 2) {
			return ndebeleHelpService.findById(subscriberSelection).getHelpAnswer();
		} else {
			return englishHelpService.findById(subscriberSelection).getHelpAnswer();
		}
	}

	private boolean isValidResponse(String responseMessage) {
		Long questionCount = englishHelpService.findAllContent().stream().count();
		if (!isValidNumericResponse(responseMessage)) {
			return false;
		}
		Long subscriberResponse = Long.parseLong(responseMessage);
		if (subscriberResponse < 0 || subscriberResponse > questionCount) {
			return false;
		}
		return true;
	}

	private boolean isValidConfirmationResponse(String responseMessage) {
		if (!isValidNumericResponse(responseMessage)) {
			return false;
		}
		Long subscriberSelection = Long.parseLong(responseMessage);
		if (subscriberSelection < 0 || subscriberSelection > 2) {
			return false;
		}
		return true;
	}

	private boolean isValidNumericResponse(String responseMessage) {
		if (isBlank(responseMessage)) {
			return false;
		}
		if (!isNumeric(responseMessage)) {
			return false;
		}
		return true;
	}

	private Long getSubscriberLanguageCode(String msisdn) {
		Optional<Long> languageID = Optional.ofNullable(farmerService.findByMsisdn(msisdn).getLanguageID());
		return languageID.orElse(3L);
	}

}
