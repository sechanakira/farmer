package com.econetwireless.ecofarmer.ussd.clubs;

import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;

public class EsokoUnsubscribeHandler implements AbstractUSSDMenuHandler {

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		return null;
	}

}
