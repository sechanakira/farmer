package com.econetwireless.ecofarmer.ussd.clubs;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_PENDING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.INVALID_OPTION_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDMenuHeaderConstants.ECOFARMER_HEADER;
import static com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler.createMessageResponse;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNumeric;

import java.util.Optional;
import java.util.function.BiFunction;

import com.econetwireless.ecofarmer.persistence.model.EcoFarmerClubsSessionData;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;

public class EcoFarmerClubsHandler implements AbstractUSSDMenuHandler {

	private USSDSessionService ussdSessionService;

	private BiFunction<MessageRequestType, USSDSession, MessageResponseType> esokoHandler;

	private BiFunction<MessageRequestType, USSDSession, MessageResponseType> zfuComboHandler;

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		Optional<EcoFarmerClubsSessionData> sessionData = Optional.ofNullable(
						ussdSession.getEcoFarmerClubsSessionData());
		if (sessionData.isPresent()) {
			if (!isValidResponse(request.getMessage().trim())) {
				return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
			}
			return findHandlerAndGetResponse(request.getMessage().trim(), request, ussdSession);
		} else {
			EcoFarmerClubsSessionData ecoFarmerClubsSessionData = new EcoFarmerClubsSessionData();
			ecoFarmerClubsSessionData.setStage(USSDSessionStage.CLUB_SELECTION);
			ussdSession.setEcoFarmerClubsSessionData(ecoFarmerClubsSessionData);
			ussdSession.setNextStage(USSDSessionStage.CLUBS);
			ussdSessionService.saveUSSDSession(ussdSession);
			return createMessageResponse(request,
							ECOFARMER_HEADER + "Select Club \n1.EcoFarmer Pamoja Fresh(Mubatsiri) \n2.ZFU Combo",
							GATEWAY_STAGE_PENDING);
		}
	}

	private boolean isValidResponse(String responseMessage) {
		if (isBlank(responseMessage)) {
			return false;
		}
		if (!isNumeric(responseMessage)) {
			return false;
		}
		Long subscriberSelection = Long.parseLong(responseMessage);
		if (subscriberSelection < 0 || subscriberSelection > 2) {
			return false;
		}
		return true;
	}

	private MessageResponseType findHandlerAndGetResponse(String selectedOption, MessageRequestType request, USSDSession ussdSession) {
		Long option = Long.parseLong(selectedOption);
		if (option == 1) {
			ussdSession.setNextStage(USSDSessionStage.ESOKO);
			ussdSessionService.saveUSSDSession(ussdSession);
			return esokoHandler.apply(request, ussdSession);
		} else {
			ussdSession.setNextStage(USSDSessionStage.ZFU);
			ussdSessionService.saveUSSDSession(ussdSession);
			return zfuComboHandler.apply(request, ussdSession);
		}
	}

	public void setUssdSessionService(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	public void setEsokoHandler(BiFunction<MessageRequestType, USSDSession, MessageResponseType> esokoHandler) {
		this.esokoHandler = esokoHandler;
	}

	public void setZfuComboHandler(BiFunction<MessageRequestType, USSDSession, MessageResponseType> zfuComboHandler) {
		this.zfuComboHandler = zfuComboHandler;
	}

}
