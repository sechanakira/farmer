package com.econetwireless.ecofarmer.ussd.clubs;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_PENDING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.INVALID_OPTION_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDMenuHeaderConstants.ECOFARMER_HEADER;
import static com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler.createMessageResponse;

import java.util.Optional;

import static org.apache.commons.lang3.StringUtils.*;

import com.econetwireless.ecofarmer.persistence.model.EsokoStageSessionData;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.service.FarmerService;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;
import com.econetwireless.ecofarmer.ussd.registration.EcoFarmerRegistrationHandler;

public class EsokoHandler implements AbstractUSSDMenuHandler {

	private USSDSessionService ussdSessionService;

	private AbstractUSSDMenuHandler esokoHelpHandler;

	private AbstractUSSDMenuHandler esokoOfferToSellHandler;

	private AbstractUSSDMenuHandler esokoSubscriptionHandler;

	private AbstractUSSDMenuHandler esokoUnsubscribeHandler;

	private FarmerService farmerService;

	private EcoFarmerRegistrationHandler ecofarmerRegistrationHandler;

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		Optional<EsokoStageSessionData> sessionData = Optional.ofNullable(
						ussdSessionService.findBySessionID(ussdSession.getSessionID()).getEsokoStageSessionData());
		if (sessionData.isPresent()) {
			if (!isValidResponse(request.getMessage().trim())) {
				return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
			}
			EsokoStageSessionData subscriberSessionData = ussdSession.getEsokoStageSessionData();
			subscriberSessionData.setSelectedEsokoOption(Long.parseLong(request.getMessage().trim()));
			ussdSession.setEsokoStageSessionData(subscriberSessionData);
			ussdSessionService.saveUSSDSession(ussdSession);
			return findHandler(ussdSession).apply(request, ussdSession);
		} else {
			if (!farmerService.isFullyRegistered(ussdSession.getMsisdn())) {
				return processRegistration(request, ussdSession);
			}
			EsokoStageSessionData esokoStageSessionData = new EsokoStageSessionData();
			ussdSession.setEsokoStageSessionData(esokoStageSessionData);
			ussdSessionService.saveUSSDSession(ussdSession);
			return createMessageResponse(request,
							ECOFARMER_HEADER + "Select Option\n1.Subscribe \n2.Offer to sell \n3.Unsubscribe \n4.Help",
							GATEWAY_STAGE_PENDING);
		}
	}
	
	private MessageResponseType processRegistration(MessageRequestType request, USSDSession ussdSession){
		return null;
	}

	private boolean isValidResponse(String response) {
		if (isBlank(response)) {
			return false;
		}
		if (isNumeric(response)) {
			return false;
		}
		Long selectedOption = Long.parseLong(response);
		if (selectedOption < 0 || selectedOption > 4) {
			return false;
		}
		return true;
	}

	private AbstractUSSDMenuHandler findHandler(USSDSession ussdSession) {
		Long selectedOption = ussdSession.getEsokoStageSessionData().getSelectedEsokoOption();
		if (selectedOption == 1) {
			return esokoSubscriptionHandler;
		} else if (selectedOption == 2) {
			return esokoOfferToSellHandler;
		} else if (selectedOption == 3) {
			return esokoUnsubscribeHandler;
		} else {
			return esokoHelpHandler;
		}
	}

	public void setUssdSessionService(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	public void setEsokoHelpHandler(AbstractUSSDMenuHandler esokoHelpHandler) {
		this.esokoHelpHandler = esokoHelpHandler;
	}

	public void setEsokoOfferToSellHandler(AbstractUSSDMenuHandler esokoOfferToSellHandler) {
		this.esokoOfferToSellHandler = esokoOfferToSellHandler;
	}

	public void setEsokoSubscriptionHandler(AbstractUSSDMenuHandler esokoSubscriptionHandler) {
		this.esokoSubscriptionHandler = esokoSubscriptionHandler;
	}

	public void setEsokoUnsubscribeHandler(AbstractUSSDMenuHandler esokoUnsubscribeHandler) {
		this.esokoUnsubscribeHandler = esokoUnsubscribeHandler;
	}

	public void setFarmerService(FarmerService farmerService) {
		this.farmerService = farmerService;
	}

	public void setEcofarmerRegistrationHandler(EcoFarmerRegistrationHandler ecofarmerRegistrationHandler) {
		this.ecofarmerRegistrationHandler = ecofarmerRegistrationHandler;
	}

}
