package com.econetwireless.ecofarmer.ussd.clubs;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_PENDING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.INVALID_OPTION_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDMenuHeaderConstants.ECOFARMER_HEADER;
import static com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler.createMessageResponse;

import static java.util.stream.Collectors.joining;

import com.econetwireless.ecofarmer.persistence.model.EsokoStageSessionData;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.service.EsokoBillingOptionService;
import com.econetwireless.ecofarmer.persistence.service.EsokoPaymentMethodService;
import com.econetwireless.ecofarmer.persistence.service.EsokoTipService;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;

import static java.util.Optional.ofNullable;

import java.util.Optional;

import static org.apache.commons.lang3.StringUtils.*;

public class EsokoSubscriptionHandler implements AbstractUSSDMenuHandler {

	private USSDSessionService ussdSessionService;

	private EsokoTipService esokoTipService;
	
	private EsokoBillingOptionService esokoBillingOptionService;
	
	private EsokoPaymentMethodService esokoPaymentMethodService;
	

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		EsokoStageSessionData sessionData = ussdSessionService.findBySessionID(
						ussdSession.getSessionID()).getEsokoStageSessionData();
		Optional<USSDSessionStage> subscriptionStage = ofNullable(sessionData.getEsokoSubscriptionStage());
		if(subscriptionStage.isPresent()){
			if(USSDSessionStage.BILLING_OPTION.equals(subscriptionStage.get())){
				return billingSelectionResponse(request, ussdSession);
			}
			else if(USSDSessionStage.PAYMENT_METHOD.equals(subscriptionStage.get())){
				return paymentMethodResponse(request, ussdSession);
			}
			else if(USSDSessionStage.SUBSCRIPTION_CONFIRMATION.equals(subscriptionStage.get())){
				return subscriptionConfirmationResponse(request, ussdSession);
			}
		}
		else{
			return tipSelectionResponse(request, ussdSession);
		}
		return null;
	}

	private MessageResponseType tipSelectionResponse(MessageRequestType request, USSDSession ussdSession) {
		String responseMessage = esokoTipService.findAllEsokoTips().stream().map(
						esokoTip -> esokoTip.getId() + "." + esokoTip.getTipName()).collect(joining("\n"));
		EsokoStageSessionData subscriberSessionData = ussdSession.getEsokoStageSessionData();
		subscriberSessionData.setEsokoSubscriptionStage(USSDSessionStage.BILLING_OPTION);
		ussdSessionService.saveUSSDSession(ussdSession);
		return createMessageResponse(request, ECOFARMER_HEADER + "Select Tip Wanted" + "\n" + responseMessage,
						GATEWAY_STAGE_PENDING);
	}
	
	private MessageResponseType billingSelectionResponse(MessageRequestType request, USSDSession ussdSession){
		if(!isValidTipSelection(request.getMessage().trim())){
			return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
		}
		String responseMessage = esokoBillingOptionService.findAllBillingOptions().stream().map(billingOption->billingOption.getId() + "." + billingOption.getBillingType()).collect(joining("\n"));
		EsokoStageSessionData subscriberSessionData = ussdSession.getEsokoStageSessionData();
		subscriberSessionData.setEsokoSubscriptionStage(USSDSessionStage.PAYMENT_METHOD);
		subscriberSessionData.setSelectedTipId(Long.parseLong(request.getMessage().trim()));
		ussdSessionService.saveUSSDSession(ussdSession);
		return createMessageResponse(request, ECOFARMER_HEADER + "Select Billing Option" + "\n" + responseMessage,
						GATEWAY_STAGE_PENDING);
	}
	
	private MessageResponseType paymentMethodResponse(MessageRequestType request, USSDSession ussdSession){
		if(!isValidBillingSelection(request.getMessage().trim())){
			return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
		}
		String responseMessage = esokoPaymentMethodService.findAllPaymentMethods().stream().map(paymentMethod->paymentMethod.getId() + "." + paymentMethod.getPaymentMethod()).collect(joining("\n")); 
		EsokoStageSessionData subscriberSessionData = ussdSession.getEsokoStageSessionData();
		subscriberSessionData.setSelectedBillingOption(Long.parseLong(request.getMessage().trim()));
		subscriberSessionData.setEsokoSubscriptionStage(USSDSessionStage.SUBSCRIPTION_CONFIRMATION);
		ussdSessionService.saveUSSDSession(ussdSession);
		return createMessageResponse(request, ECOFARMER_HEADER + "Select Payment Method" + "\n" + responseMessage,
						GATEWAY_STAGE_PENDING);
	}
	
	private MessageResponseType subscriptionConfirmationResponse(MessageRequestType request, USSDSession ussdSession){
		if(!isValidPaymentMethod(request.getMessage().trim())){
			return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
		}
		EsokoStageSessionData subscriberSessionData = ussdSession.getEsokoStageSessionData();
		subscriberSessionData.setSelectedPaymentOption(Long.parseLong(request.getMessage().trim()));
		subscriberSessionData.setEsokoSubscriptionStage(USSDSessionStage.ESOKO_FINAL_STAGE);
		ussdSessionService.saveUSSDSession(ussdSession);
		String responseMessage = "Confirm .......";
		return createMessageResponse(request, ECOFARMER_HEADER + responseMessage,
						GATEWAY_STAGE_PENDING);
	}
	
	private boolean isValidPaymentMethod(String selectedPaymentMethod){
		if(isBlank(selectedPaymentMethod)){
			return false;
		}
		if(!isNumeric(selectedPaymentMethod)){
			return false;
		}
		Long paymentMethodID = Long.parseLong(selectedPaymentMethod);
		Long maxPaymentID = esokoPaymentMethodService.findAllPaymentMethods().stream().count();
		if(paymentMethodID < 0 || paymentMethodID > maxPaymentID){
			return false;
		}
		return true;
	}
	
	private boolean isValidBillingSelection(String selectedBillingOption){
		if(isBlank(selectedBillingOption)){
			return false;
		}
		if(!isNumeric(selectedBillingOption)){
			return false;
		}
		Long maxBillingID = esokoBillingOptionService.findAllBillingOptions().stream().count(); 
		Long billingOptionID = Long.parseLong(selectedBillingOption);
		if(billingOptionID < 0 || billingOptionID > maxBillingID){
			return false;
		}
		return true;
	}
	
	private boolean isValidTipSelection(String tipSelection){
		if(isBlank(tipSelection)){
			return false;
		}
		if(!isNumeric(tipSelection)){
			return false;
		}
		Long selectedTipId = Long.parseLong(tipSelection);
		Long maxTipId = esokoTipService.findAllEsokoTips().stream().count();
		if(selectedTipId < 0 || selectedTipId > maxTipId){
			return false;
		}
		return true;
	}

	public void setUssdSessionService(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	public void setEsokoTipService(EsokoTipService esokoTipService) {
		this.esokoTipService = esokoTipService;
	}

}
