package com.econetwireless.ecofarmer.ussd.registration;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_PENDING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.INVALID_OPTION_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.THANK_YOU_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDMenuHeaderConstants.ECOFARMER_HEADER;
import static com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler.createMessageResponse;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNumeric;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.econetwireless.ecofarmer.persistence.model.Farmer;
import com.econetwireless.ecofarmer.persistence.model.Language;
import com.econetwireless.ecofarmer.persistence.model.LanguageSelectionSessionData;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.service.FarmerService;
import com.econetwireless.ecofarmer.persistence.service.LanguageService;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.sms.SMSBean;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;

public class LanguageSelectionHandler implements AbstractUSSDMenuHandler {

	private FarmerService farmerService;

	private USSDSessionService ussdSessionService;

	private LanguageService languageService;

	private SMSBean smsBean;

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		Optional<LanguageSelectionSessionData> languageSelectionSessionData = Optional.ofNullable(
						ussdSession.getLanguageSelectionSessionData());
		if (languageSelectionSessionData.isPresent()) {
			if (isValidLanguageSelection(request.getMessage().trim())) {
				Farmer farmer = farmerService.findByMsisdn(ussdSession.getMsisdn());
				farmer.setRegistrationDate(LocalDateTime.now());
				farmer.setLanguageID(Long.parseLong(request.getMessage().trim()));
				farmerService.saveFarmer(farmer);
				smsBean.sendSMS(ussdSession.getMsisdn(), THANK_YOU_MESSAGE);
				return createMessageResponse(request, THANK_YOU_MESSAGE, GATEWAY_STAGE_COMPLETE);
			} else {
				return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
			}
		} else {
			String subscriberMessage = ECOFARMER_HEADER + "Select Language: \n" + languageService.findAllLanguages().stream().map(
							language -> language.getLanguageID() + "." + language.getLanguageName()).collect(
											joining("\n"));
			LanguageSelectionSessionData sessionData = new LanguageSelectionSessionData();
			sessionData.setStage(USSDSessionStage.CONFIRM_LANGUAGE_SELECTION);
			ussdSession.setLanguageSelectionSessionData(sessionData);
			ussdSessionService.saveUSSDSession(ussdSession);
			return createMessageResponse(request, subscriberMessage, GATEWAY_STAGE_PENDING);
		}
	}

	public void setFarmerService(FarmerService farmerService) {
		this.farmerService = farmerService;
	}

	public void setUssdSessionService(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	public void setLanguageService(LanguageService languageService) {
		this.languageService = languageService;
	}

	public void setSmsBean(SMSBean smsBean) {
		this.smsBean = smsBean;
	}

	private boolean isValidLanguageSelection(String selectedLanguage) {
		if (isBlank(selectedLanguage)) {
			return false;
		}
		if (!isNumeric(selectedLanguage)) {
			return false;
		}
		List<Long> validLanguageIDs = languageService.findAllLanguages().stream().map(Language::getLanguageID).collect(
						toList());
		if (!validLanguageIDs.contains(Long.parseLong(selectedLanguage))) {
			return false;
		}
		return true;
	}

}
