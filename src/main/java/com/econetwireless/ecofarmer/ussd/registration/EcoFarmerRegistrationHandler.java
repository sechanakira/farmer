package com.econetwireless.ecofarmer.ussd.registration;

import java.util.Optional;

import com.econetwireless.ecofarmer.persistence.model.EcoCashCustomerData;
import com.econetwireless.ecofarmer.persistence.model.Farmer;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.service.EcoCashCustomerDataService;
import com.econetwireless.ecofarmer.persistence.service.FarmerService;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;

public class EcoFarmerRegistrationHandler implements AbstractUSSDMenuHandler {

	private FarmerService farmerService;

	private EcoCashCustomerDataService ecoCashCustomerDataService;

	private DistrictSelectionHandler districtSelectionHandler;

	private EcocashDataHandler ecoCashCustomerDataHandler;

	private ProvinceSelectionHandler provinceSelectionHandler;

	private WardSelectionHandler wardSelectionHandler;

	private FarmTypeSelectionHandler farmTypeSelectionHandler;

	private LanguageSelectionHandler languageSelectionHandler;

	private USSDSessionService ussdSessionService;

	private ViewRegistrationDetailsHandler viewRegistrationDetailsHandler;

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession session) {
		session.setNextStage(USSDSessionStage.REGISTRATION);
		ussdSessionService.saveUSSDSession(session);
		Optional<Farmer> farmer = Optional.ofNullable(farmerService.findByMsisdn(session.getMsisdn()));
		if (farmer.isPresent()) {
			Optional<EcoCashCustomerData> ecoCashData = Optional.ofNullable(
							ecoCashCustomerDataService.findByMSISDN(request.getSourceNumber()));
			if (ecoCashData.isPresent()) {
				Farmer subscriber = farmer.get();
				if (subscriber.getProvinceID() == null) {
					return provinceSelectionHandler.apply(request, session);
				}
				if (subscriber.getDistrictID() == null) {
					return districtSelectionHandler.apply(request, session);
				}
				if (subscriber.getWardNumber() == null) {
					return wardSelectionHandler.apply(request, session);
				}
				if (subscriber.getFarmTypeID() == null) {
					return farmTypeSelectionHandler.apply(request, session);
				}
				if (subscriber.getLanguageID() == null) {
					return languageSelectionHandler.apply(request, session);
				}
			} else {
				return ecoCashCustomerDataHandler.apply(request, session);
			}
		} else {
			Farmer subscriber = new Farmer();
			subscriber.setMsisdn(request.getSourceNumber());
			farmerService.saveFarmer(subscriber);
			return ecoCashCustomerDataHandler.apply(request, session);
		}
		return viewRegistrationDetailsHandler.apply(request, session);
	}

	public void setFarmerService(FarmerService farmerService) {
		this.farmerService = farmerService;
	}

	public void setEcoCashCustomerDataService(EcoCashCustomerDataService ecoCashCustomerDataService) {
		this.ecoCashCustomerDataService = ecoCashCustomerDataService;
	}

	public void setDistrictSelectionHandler(DistrictSelectionHandler districtSelectionHandler) {
		this.districtSelectionHandler = districtSelectionHandler;
	}

	public void setEcoCashCustomerDataHandler(EcocashDataHandler ecoCashCustomerDataHandler) {
		this.ecoCashCustomerDataHandler = ecoCashCustomerDataHandler;
	}

	public void setProvinceSelectionHandler(ProvinceSelectionHandler provinceSelectionHandler) {
		this.provinceSelectionHandler = provinceSelectionHandler;
	}

	public void setWardSelectionHandler(WardSelectionHandler wardSelectionHandler) {
		this.wardSelectionHandler = wardSelectionHandler;
	}

	public void setUssdSessionService(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	public void setFarmTypeSelectionHandler(FarmTypeSelectionHandler farmTypeSelectionHandler) {
		this.farmTypeSelectionHandler = farmTypeSelectionHandler;
	}

	public void setLanguageSelectionHandler(LanguageSelectionHandler languageSelectionHandler) {
		this.languageSelectionHandler = languageSelectionHandler;
	}

	public void setViewRegistrationDetailsHandler(ViewRegistrationDetailsHandler viewRegistrationDetailsHandler) {
		this.viewRegistrationDetailsHandler = viewRegistrationDetailsHandler;
	}

}
