package com.econetwireless.ecofarmer.ussd.registration;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.DistrictSelectionConstants.MAX_MENU_ITEMS;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.DistrictSelectionConstants.NEXT_MENU_LIST_SELECTION;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_PENDING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.INVALID_OPTION_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDMenuHeaderConstants.ECOFARMER_HEADER;
import static com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler.createMessageResponse;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNumeric;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

import com.econetwireless.ecofarmer.persistence.model.District;
import com.econetwireless.ecofarmer.persistence.model.DistrictSelectionSessionData;
import com.econetwireless.ecofarmer.persistence.model.Farmer;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.service.DistrictService;
import com.econetwireless.ecofarmer.persistence.service.FarmerService;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;

public class DistrictSelectionHandler implements AbstractUSSDMenuHandler {

	private BiFunction<MessageRequestType, USSDSession, MessageResponseType> successor;

	private FarmerService farmerService;

	private USSDSessionService ussdSessionService;

	private DistrictService districtService;

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		Optional<DistrictSelectionSessionData> districtSelectionSessionData = Optional.ofNullable(
						ussdSession.getDistrictSelectionSessionData());
		if (districtSelectionSessionData.isPresent()) {
			Long districtCount = districtService.findDistrictsByProvinceID(
							farmerService.findByMsisdn(ussdSession.getMsisdn()).getProvinceID()).stream().count();
			if (districtSelectionSessionData.get().getStage().equals(
							USSDSessionStage.CHECK_DITRICT_SELECTION) && NEXT_MENU_LIST_SELECTION.equals(
											request.getMessage().trim()) && districtCount > MAX_MENU_ITEMS) {
				List<District> provinceDistricts = districtService.findDistrictsByProvinceID(
								farmerService.findByMsisdn(ussdSession.getMsisdn()).getProvinceID());
				List<District> remainingProvinceDistricts = provinceDistricts.subList(MAX_MENU_ITEMS,
								provinceDistricts.size());
				String subscriberMessage = ECOFARMER_HEADER + "Select District: \n" + remainingProvinceDistricts.stream().map(
								province -> province.getDistrictID() + "." + province.getDistrictName()).collect(
												joining("\n"));
				return createMessageResponse(request, subscriberMessage, GATEWAY_STAGE_PENDING);
			} else {
				if (isValidDistrictSelection(request.getMessage().trim(), ussdSession.getMsisdn())) {
					Farmer farmer = farmerService.findByMsisdn(ussdSession.getMsisdn());
					farmer.setDistrictID(Long.parseLong(request.getMessage().trim()));
					farmerService.saveFarmer(farmer);
					return successor.apply(request, ussdSession);
				} else {
					return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
				}
			}
		} else {
			DistrictSelectionSessionData subscriberDistrictSessionData = new DistrictSelectionSessionData();
			List<District> provinceDistricts = districtService.findDistrictsByProvinceID(
							farmerService.findByMsisdn(ussdSession.getMsisdn()).getProvinceID());
			String subscriberMessage = ECOFARMER_HEADER + "Select District: \n";
			if (provinceDistricts.size() <= MAX_MENU_ITEMS) {
				subscriberMessage += provinceDistricts.stream().map(
								district -> district.getDistrictID() + "." + district.getDistrictName()).collect(
												joining("\n"));
			} else {
				subscriberMessage += provinceDistricts.stream().map(
								district -> district.getDistrictID() + "." + district.getDistrictName()).limit(
												MAX_MENU_ITEMS).collect(joining("\n"));
				subscriberMessage += "\n0.Next";
			}
			subscriberDistrictSessionData.setStage(USSDSessionStage.CHECK_DITRICT_SELECTION);
			ussdSession.setDistrictSelectionSessionData(subscriberDistrictSessionData);
			ussdSessionService.saveUSSDSession(ussdSession);
			return createMessageResponse(request, subscriberMessage, GATEWAY_STAGE_PENDING);
		}
	}

	public void setSuccessor(BiFunction<MessageRequestType, USSDSession, MessageResponseType> successor) {
		this.successor = successor;
	}

	public void setFarmerService(FarmerService farmerService) {
		this.farmerService = farmerService;
	}

	public void setUssdSessionService(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	public void setDistrictService(DistrictService districtService) {
		this.districtService = districtService;
	}

	private boolean isValidDistrictSelection(String selectedDistrict, String msisdn) {
		if (isBlank(selectedDistrict)) {
			return false;
		}
		if (!isNumeric(selectedDistrict)) {
			return false;
		}
		List<Long> validDistrictIDs = districtService.findDistrictsByProvinceID(
						farmerService.findByMsisdn(msisdn).getProvinceID()).stream().map(
										District::getDistrictID).collect(toList());
		if (!validDistrictIDs.contains(Long.parseLong(selectedDistrict))) {
			return false;
		}
		return true;
	}

}
