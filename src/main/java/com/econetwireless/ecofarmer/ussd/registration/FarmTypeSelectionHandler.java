package com.econetwireless.ecofarmer.ussd.registration;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_PENDING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.INVALID_OPTION_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDMenuHeaderConstants.ECOFARMER_HEADER;
import static com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler.createMessageResponse;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNumeric;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

import com.econetwireless.ecofarmer.persistence.model.FarmType;
import com.econetwireless.ecofarmer.persistence.model.FarmTypeSelectionSessionData;
import com.econetwireless.ecofarmer.persistence.model.Farmer;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.service.FarmTypeService;
import com.econetwireless.ecofarmer.persistence.service.FarmerService;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;

public class FarmTypeSelectionHandler implements AbstractUSSDMenuHandler {

	private BiFunction<MessageRequestType, USSDSession, MessageResponseType> successor;

	private FarmerService farmerService;

	private USSDSessionService ussdSessionService;

	private FarmTypeService farmTypeService;

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		Optional<FarmTypeSelectionSessionData> farmTypeSelectionSessionData = Optional.ofNullable(
						ussdSession.getFarmTypeSelectionSessionData());
		if (farmTypeSelectionSessionData.isPresent()) {
			if (isValidFarmTypeSelection(request.getMessage().trim())) {
				Farmer farmer = farmerService.findByMsisdn(ussdSession.getMsisdn());
				farmer.setFarmTypeID(Long.parseLong(request.getMessage().trim()));
				farmerService.saveFarmer(farmer);
				return successor.apply(request, ussdSession);
			} else {
				return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
			}
		} else {
			String subscriberMessage = ECOFARMER_HEADER + "Select Farm Type: \n" + farmTypeService.findAllFarmTypes().stream().map(
							farmType -> farmType.getFarmTypeID() + "." + farmType.getFarmTypeName()).collect(
											joining("\n"));
			FarmTypeSelectionSessionData sessionData = new FarmTypeSelectionSessionData();
			sessionData.setStage(USSDSessionStage.CONFIRM_FARM_TYPE_SELECTION);
			ussdSession.setFarmTypeSelectionSessionData(sessionData);
			ussdSessionService.saveUSSDSession(ussdSession);
			return createMessageResponse(request, subscriberMessage, GATEWAY_STAGE_PENDING);
		}
	}

	public void setSuccessor(BiFunction<MessageRequestType, USSDSession, MessageResponseType> successor) {
		this.successor = successor;
	}

	public void setFarmerService(FarmerService farmerService) {
		this.farmerService = farmerService;
	}

	public void setUssdSessionService(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	public void setFarmTypeService(FarmTypeService farmTypeService) {
		this.farmTypeService = farmTypeService;
	}

	private boolean isValidFarmTypeSelection(String selectedFarmType) {
		if (isBlank(selectedFarmType)) {
			return false;
		}
		if (!isNumeric(selectedFarmType)) {
			return false;
		}
		List<Long> validFarmTypeIDs = farmTypeService.findAllFarmTypes().stream().map(FarmType::getFarmTypeID).collect(
						toList());
		if (!validFarmTypeIDs.contains(Long.parseLong(selectedFarmType))) {
			return false;
		}
		return true;
	}

}
