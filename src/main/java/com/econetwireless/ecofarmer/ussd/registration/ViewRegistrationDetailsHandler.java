package com.econetwireless.ecofarmer.ussd.registration;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDMenuHeaderConstants.ECOFARMER_HEADER;
import static com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler.createMessageResponse;

import com.econetwireless.ecofarmer.persistence.model.Farmer;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.service.DistrictService;
import com.econetwireless.ecofarmer.persistence.service.FarmerService;
import com.econetwireless.ecofarmer.persistence.service.LanguageService;
import com.econetwireless.ecofarmer.persistence.service.ProvinceService;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;

public class ViewRegistrationDetailsHandler implements AbstractUSSDMenuHandler {

	private DistrictService districtService;

	private ProvinceService provinceService;

	private FarmerService farmerService;

	private LanguageService languageService;

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		return createMessageResponse(request, getRegistrationDetails(ussdSession.getMsisdn()), GATEWAY_STAGE_COMPLETE);
	}

	public void setDistrictService(DistrictService districtService) {
		this.districtService = districtService;
	}

	public void setProvinceService(ProvinceService provinceService) {
		this.provinceService = provinceService;
	}

	public void setFarmerService(FarmerService farmerService) {
		this.farmerService = farmerService;
	}

	public void setLanguageService(LanguageService languageService) {
		this.languageService = languageService;
	}

	private String getRegistrationDetails(String msisdn) {
		Farmer subscriber = farmerService.findByMsisdn(msisdn);
		String subscriberMessage = ECOFARMER_HEADER + "EcoFarmer Registration Details:\n";
		subscriberMessage += "Province: " + provinceService.findProvinceByID(
						subscriber.getProvinceID()).getProvinceName() + "\n";
		subscriberMessage += "District: " + districtService.findDistrictByID(
						subscriber.getDistrictID()).getDistrictName() + "\n";
		subscriberMessage += "Ward: " + subscriber.getWardNumber() + "\n";
		subscriberMessage += "Language: " + languageService.findByLanguageID(
						subscriber.getLanguageID()).getLanguageName();
		return subscriberMessage;
	}

}
