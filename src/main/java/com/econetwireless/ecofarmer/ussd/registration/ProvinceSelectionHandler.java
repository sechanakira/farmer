package com.econetwireless.ecofarmer.ussd.registration;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_PENDING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.INVALID_OPTION_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDMenuHeaderConstants.ECOFARMER_HEADER;
import static com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler.createMessageResponse;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNumeric;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

import com.econetwireless.ecofarmer.persistence.model.Farmer;
import com.econetwireless.ecofarmer.persistence.model.Province;
import com.econetwireless.ecofarmer.persistence.model.ProvinceSelectionSessionData;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.service.FarmerService;
import com.econetwireless.ecofarmer.persistence.service.ProvinceService;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;

public class ProvinceSelectionHandler implements AbstractUSSDMenuHandler {

	private BiFunction<MessageRequestType, USSDSession, MessageResponseType> successor;

	private ProvinceService provinceService;

	private FarmerService farmerService;

	private USSDSessionService ussdSessionService;

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		Optional<ProvinceSelectionSessionData> provinceSelectionData = Optional.ofNullable(
						ussdSessionService.findBySessionID(
										request.getTransactionID()).getProvinceSelectionSessionData());
		if (provinceSelectionData.isPresent()) {
			if (isValidProvinceSelection(request.getMessage().trim())) {
				Farmer farmer = farmerService.findByMsisdn(ussdSession.getMsisdn());
				farmer.setProvinceID(Long.parseLong(request.getMessage().trim()));
				farmerService.saveFarmer(farmer);
				return successor.apply(request, ussdSession);
			} else {
				return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
			}
		} else {
			String subscriberMessage = ECOFARMER_HEADER + "Select province:\n" + provinceService.findAllProvinces().stream().map(
							province -> province.getProvinceID() + "." + province.getProvinceName()).collect(
											joining("\n"));
			ProvinceSelectionSessionData subscriberProvinceSelectionData = new ProvinceSelectionSessionData();
			subscriberProvinceSelectionData.setStage(USSDSessionStage.PROVINCE_SELECTION_CHECK);
			ussdSession.setProvinceSelectionSessionData(subscriberProvinceSelectionData);
			ussdSessionService.saveUSSDSession(ussdSession);
			return createMessageResponse(request, subscriberMessage, GATEWAY_STAGE_PENDING);
		}
	}

	public BiFunction<MessageRequestType, USSDSession, MessageResponseType> getSuccessor() {
		return successor;
	}

	public void setSuccessor(BiFunction<MessageRequestType, USSDSession, MessageResponseType> successor) {
		this.successor = successor;
	}

	public void setProvinceService(ProvinceService provinceService) {
		this.provinceService = provinceService;
	}

	public void setFarmerService(FarmerService farmerService) {
		this.farmerService = farmerService;
	}

	public void setUssdSessionService(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	private boolean isValidProvinceSelection(String provinceSelection) {
		if (isBlank(provinceSelection)) {
			return false;
		}
		if (!isNumeric(provinceSelection)) {
			return false;
		}
		Long selectedProvinceID = Long.parseLong(provinceSelection);
		List<Long> validProvinceIDs = provinceService.findAllProvinces().stream().map(Province::getProvinceID).collect(
						toList());
		if (!validProvinceIDs.contains(selectedProvinceID)) {
			return false;
		}
		return true;
	}

}
