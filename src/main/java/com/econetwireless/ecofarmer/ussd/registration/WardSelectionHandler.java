package com.econetwireless.ecofarmer.ussd.registration;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_PENDING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.INVALID_OPTION_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDMenuHeaderConstants.ECOFARMER_HEADER;
import static com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler.createMessageResponse;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNumeric;

import java.util.Optional;
import java.util.function.BiFunction;

import com.econetwireless.ecofarmer.persistence.model.District;
import com.econetwireless.ecofarmer.persistence.model.Farmer;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.model.WardSelectionSessionData;
import com.econetwireless.ecofarmer.persistence.service.DistrictService;
import com.econetwireless.ecofarmer.persistence.service.FarmerService;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;

public class WardSelectionHandler implements AbstractUSSDMenuHandler {

	private BiFunction<MessageRequestType, USSDSession, MessageResponseType> successor;

	private FarmerService farmerService;

	private USSDSessionService ussdSessionService;

	private DistrictService districtService;

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		Optional<WardSelectionSessionData> wardSelectionSessionData = Optional.ofNullable(
						ussdSession.getWardSelectionSessionData());
		if (wardSelectionSessionData.isPresent()) {
			if (isValidWardNumber(request.getMessage().trim(), ussdSession.getMsisdn())) {
				Farmer farmer = farmerService.findByMsisdn(ussdSession.getMsisdn());
				farmer.setWardNumber(Long.parseLong(request.getMessage().trim()));
				farmerService.saveFarmer(farmer);
				return successor.apply(request, ussdSession);
			} else {
				return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
			}
		} else {
			WardSelectionSessionData subscriberWardSelectionSessionData = new WardSelectionSessionData();
			subscriberWardSelectionSessionData.setStage(USSDSessionStage.CONFIRM_WARD_SELECTION);
			ussdSession.setWardSelectionSessionData(subscriberWardSelectionSessionData);
			ussdSessionService.saveUSSDSession(ussdSession);
			return createMessageResponse(request, ECOFARMER_HEADER + "Enter your ward number:\n",
							GATEWAY_STAGE_PENDING);
		}
	}

	public void setSuccessor(BiFunction<MessageRequestType, USSDSession, MessageResponseType> successor) {
		this.successor = successor;
	}

	public void setFarmerService(FarmerService farmerService) {
		this.farmerService = farmerService;
	}

	public void setUssdSessionService(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	public void setDistrictService(DistrictService districtService) {
		this.districtService = districtService;
	}

	private boolean isValidWardNumber(String selectedWardNumber, String msisdn) {
		if (isBlank(selectedWardNumber)) {
			return false;
		}
		if (!isNumeric(selectedWardNumber)) {
			return false;
		}
		District subscriberDistrict = districtService.findDistrictByID(
						farmerService.findByMsisdn(msisdn).getDistrictID());
		if (Long.parseLong(selectedWardNumber) > subscriberDistrict.getNumberOfWards()) {
			return false;
		}
		if (subscriberDistrict.getDistrictID() == 2 && Long.parseLong(selectedWardNumber) == 1) {
			return false;
		}
		return true;
	}

}
