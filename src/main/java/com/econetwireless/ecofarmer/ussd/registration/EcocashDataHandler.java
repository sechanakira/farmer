package com.econetwireless.ecofarmer.ussd.registration;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.CANCEL_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_PENDING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.INVALID_OPTION_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.NationalIDConstants.FIFTH_ID_DIGIT;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.NationalIDConstants.FIRST_ID_DIGIT;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDMenuHeaderConstants.ECOFARMER_HEADER;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDSelectionConstants.ACCEPT_OPTION;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDSelectionConstants.CANCEL_OPTION;
import static com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler.createMessageResponse;

import java.util.Optional;
import java.util.function.BiFunction;

import com.econetwireless.ecofarmer.persistence.model.EcoCashCustomerData;
import com.econetwireless.ecofarmer.persistence.model.RegistationStageSessionData;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.service.EcoCashCustomerDataService;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;

public class EcocashDataHandler implements AbstractUSSDMenuHandler {

	private USSDSessionService ussdSessionService;

	private EcoCashCustomerDataService ecoCashCustomerDataService;

	private BiFunction<MessageRequestType, USSDSession, MessageResponseType> successor;

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		USSDSession subscriberUSSDsession = ussdSessionService.findBySessionID(request.getTransactionID());
		Optional<RegistationStageSessionData> regState = Optional.ofNullable(
						subscriberUSSDsession.getRegistrationStage());
		if (regState.isPresent()) {
			if (regState.get().getStage().equals(USSDSessionStage.CONFIRM_ECOCASH_DATA)) {
				Optional<EcoCashCustomerData> ecocashCustomerData = Optional.ofNullable(
								ecoCashCustomerDataService.findCustomerInEcoCash(ussdSession.getMsisdn()));
				if (ecocashCustomerData.isPresent()) {
					EcoCashCustomerData subscriberCustomerData = ecocashCustomerData.get();
					String firstFourDigitsOfID = subscriberCustomerData.getNationalID().substring(FIRST_ID_DIGIT,
									FIFTH_ID_DIGIT);
					if (!firstFourDigitsOfID.equals(request.getMessage().trim())) {
						return createMessageResponse(request, "Invalid details entered", GATEWAY_STAGE_COMPLETE);
					}
					String subscriberMessage = ECOFARMER_HEADER + "Confirm EcoCash details: \n" + "First Name: " + subscriberCustomerData.getFirstName() + "\n" + "Last Name: " + subscriberCustomerData.getLastName() + "\n" + "National ID: " + subscriberCustomerData.getNationalID() + "\n" + "1.Confirm \n2.Cancel";
					subscriberUSSDsession.getRegistrationStage().setStage(
									USSDSessionStage.CHECK_ECOCASH_DATA_CONFIRMATION);
					ussdSessionService.saveUSSDSession(subscriberUSSDsession);
					return createMessageResponse(request, subscriberMessage, GATEWAY_STAGE_PENDING);
				} else {
					return createMessageResponse(request,
									"Please visit your nearest Econet Shop to verify your EcoCash account",
									GATEWAY_STAGE_COMPLETE);
				}
			}
			if (regState.get().getStage().equals(USSDSessionStage.CHECK_ECOCASH_DATA_CONFIRMATION)) {
				String subscriberResponse = request.getMessage().trim();
				if (subscriberResponse.equals(ACCEPT_OPTION)) {
					ecoCashCustomerDataService.findAndSaveEcoCashData(ussdSession.getMsisdn());
					return successor.apply(request, ussdSession);
				} else if (subscriberResponse.equals(CANCEL_OPTION)) {
					return createMessageResponse(request, CANCEL_MESSAGE, GATEWAY_STAGE_COMPLETE);
				} else {
					return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
				}
			}
		} else {
			RegistationStageSessionData registationStageSessionData = new RegistationStageSessionData();
			registationStageSessionData.setStage(USSDSessionStage.CONFIRM_ECOCASH_DATA);
			String subscriberMessage = ECOFARMER_HEADER + "Please enter the first 4 digits of your National ID (eg 5222)";
			subscriberUSSDsession.setRegistrationStage(registationStageSessionData);
			ussdSessionService.saveUSSDSession(subscriberUSSDsession);
			return createMessageResponse(request, subscriberMessage, GATEWAY_STAGE_PENDING);
		}
		return null;
	}

	public void setUssdSessionService(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	public void setEcoCashCustomerDataService(EcoCashCustomerDataService ecoCashCustomerDataService) {
		this.ecoCashCustomerDataService = ecoCashCustomerDataService;
	}

	public void setSuccessor(BiFunction<MessageRequestType, USSDSession, MessageResponseType> successor) {
		this.successor = successor;
	}

}
