package com.econetwireless.ecofarmer.ussd.bundles;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.UNAVAILABLE_OPTION;
import static com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler.createMessageResponse;

import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;

public class BuyBundlesHandler implements AbstractUSSDMenuHandler {

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		return createMessageResponse(request, UNAVAILABLE_OPTION, GATEWAY_STAGE_COMPLETE);
	}

}
