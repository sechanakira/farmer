package com.econetwireless.ecofarmer.ussd;

import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;

@FunctionalInterface
public interface USSDRequestProcessor {

	MessageResponseType processUSSDRequest(MessageRequestType ussdRequest);
}
