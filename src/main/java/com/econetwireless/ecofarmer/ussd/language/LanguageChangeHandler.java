package com.econetwireless.ecofarmer.ussd.language;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.CANCEL_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_PENDING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.INVALID_OPTION_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDMenuHeaderConstants.ECOFARMER_HEADER;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDSelectionConstants.ACCEPT_OPTION;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDSelectionConstants.CANCEL_OPTION;
import static com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler.createMessageResponse;
import static java.util.stream.Collectors.joining;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNumeric;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import com.econetwireless.ecofarmer.persistence.model.Farmer;
import com.econetwireless.ecofarmer.persistence.model.Language;
import com.econetwireless.ecofarmer.persistence.model.LanguageSelectionSessionData;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.service.FarmerService;
import com.econetwireless.ecofarmer.persistence.service.LanguageService;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;
import com.econetwireless.ecofarmer.ussd.registration.EcoFarmerRegistrationHandler;

public class LanguageChangeHandler implements AbstractUSSDMenuHandler {

	private USSDSessionService ussdSessionService;

	private LanguageService languageService;

	private FarmerService farmerService;

	private EcoFarmerRegistrationHandler ecoFarmerRegistrationHandler;

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		ussdSession.setNextStage(USSDSessionStage.LANGUAGE);
		Optional<LanguageSelectionSessionData> subscriberSessionData = Optional.ofNullable(
						ussdSession.getLanguageSelectionSessionData());
		Optional<Farmer> farmer = Optional.ofNullable(farmerService.findByMsisdn(ussdSession.getMsisdn()));
		LanguageSelectionSessionData languageSelectionSessionData = new LanguageSelectionSessionData();
		if (subscriberSessionData.isPresent()) {
			if (ussdSession.getLanguageSelectionSessionData().getStage().equals(
							USSDSessionStage.CONFIRM_LANGUAGE_SELECTION)) {
				if (isValidLanguageSelection(request.getMessage().trim())) {
					LanguageSelectionSessionData subscriberLanguageSessionData = ussdSession.getLanguageSelectionSessionData();
					subscriberLanguageSessionData.setNewLanguageSelection(request.getMessage().trim());
					subscriberLanguageSessionData.setStage(USSDSessionStage.LANGUAGE_CHANGE_FINAL_STEP);
					ussdSessionService.saveUSSDSession(ussdSession);
					return createMessageResponse(request,
									ECOFARMER_HEADER + "Confirm language change: \n1.Confirm \n2.Cancel",
									GATEWAY_STAGE_PENDING);
				} else {
					return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
				}
			}
			if (ussdSession.getLanguageSelectionSessionData().getStage().equals(
							USSDSessionStage.LANGUAGE_CHANGE_FINAL_STEP)) {
				if (ACCEPT_OPTION.equals(request.getMessage().trim())) {
					Farmer subscriber = farmerService.findByMsisdn(ussdSession.getMsisdn());
					Long currentLanguageID = farmerService.findByMsisdn(ussdSession.getMsisdn()).getLanguageID();
					List<Language> languages = languageService.findAllLanguages();
					languages.remove(languageService.findByLanguageID(currentLanguageID));
					Long newLanguageID = languages.get(Integer.parseInt(
									ussdSession.getLanguageSelectionSessionData().getNewLanguageSelection()) - 1).getLanguageID();
					subscriber.setLanguageID(newLanguageID);
					farmerService.saveFarmer(subscriber);
					return createMessageResponse(request,
									"You have successfully changed your language, thank you for using EcoFarmer",
									GATEWAY_STAGE_COMPLETE);
				} else if (CANCEL_OPTION.equals(request.getMessage().trim())) {
					return createMessageResponse(request, CANCEL_MESSAGE, GATEWAY_STAGE_COMPLETE);
				} else {
					return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
				}
			}
		}
		if (farmer.isPresent() && farmer.get().getLanguageID() != null) {
			if (ussdSession.getLanguageSelectionSessionData() != null && ussdSession.getLanguageSelectionSessionData().getStage().equals(
							USSDSessionStage.COMPLETE_REGISTRATION)) {
				if (ACCEPT_OPTION.equals(request.getMessage().trim())) {
					return ecoFarmerRegistrationHandler.apply(request, ussdSession);
				} else if (CANCEL_OPTION.equals(request.getMessage().trim())) {
					return createMessageResponse(request, CANCEL_MESSAGE, GATEWAY_STAGE_COMPLETE);
				} else {
					return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
				}
			} else {
				languageSelectionSessionData.setStage(USSDSessionStage.CONFIRM_LANGUAGE_SELECTION);
				ussdSession.setLanguageSelectionSessionData(languageSelectionSessionData);
				ussdSessionService.saveUSSDSession(ussdSession);
				Long currentLanguageID = farmerService.findByMsisdn(ussdSession.getMsisdn()).getLanguageID();
				List<Language> languages = languageService.findAllLanguages();
				languages.remove(languageService.findByLanguageID(currentLanguageID));
				AtomicInteger atomicInteger = new AtomicInteger(0);
				String subscriberMessage = ECOFARMER_HEADER + "Select new language: \n" + languages.stream().map(
								language -> atomicInteger.incrementAndGet() + "." + language.getLanguageName()).collect(
												joining("\n"));
				return createMessageResponse(request, subscriberMessage, GATEWAY_STAGE_PENDING);
			}
		} else {
			languageSelectionSessionData.setStage(USSDSessionStage.COMPLETE_REGISTRATION);
			ussdSession.setLanguageSelectionSessionData(languageSelectionSessionData);
			ussdSessionService.saveUSSDSession(ussdSession);
			return createMessageResponse(request,
							"You are not registered on EcoFarmer or your registraion is not complete. Would you like to complete registration now? \n1.Complete Registration \n2.Cancel",
							GATEWAY_STAGE_PENDING);
		}
	}

	public void setUssdSessionService(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	public void setLanguageService(LanguageService languageService) {
		this.languageService = languageService;
	}

	public void setFarmerService(FarmerService farmerService) {
		this.farmerService = farmerService;
	}

	public void setEcoFarmerRegistrationHandler(EcoFarmerRegistrationHandler ecoFarmerRegistrationHandler) {
		this.ecoFarmerRegistrationHandler = ecoFarmerRegistrationHandler;
	}

	private boolean isValidLanguageSelection(String selectedLanguage) {
		if (isBlank(selectedLanguage)) {
			return false;
		}
		if (!isNumeric(selectedLanguage)) {
			return false;
		}
		Long index = Long.parseLong(selectedLanguage);
		if (index <= 0 || index > 2) {
			return false;
		}
		return true;
	}

}
