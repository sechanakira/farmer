package com.econetwireless.ecofarmer.ussd.infoservices;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.ApplicationNames.ECOFARMER_TIPS;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.CANCEL_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_PENDING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.INVALID_OPTION_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDSelectionConstants.ACCEPT_OPTION;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDSelectionConstants.CANCEL_OPTION;
import static com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler.createMessageResponse;

import java.util.Optional;

import com.econetwireless.ecofarmer.persistence.model.InformationServicesSessionData;
import com.econetwireless.ecofarmer.persistence.model.RoutingSessionData;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.service.FarmerService;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.routing.USSDRoutingBean;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.AbstractUSSDMenuHandler;
import com.econetwireless.ecofarmer.ussd.registration.EcoFarmerRegistrationHandler;

public class InformationServicesHandler implements AbstractUSSDMenuHandler {

	private USSDRoutingBean ussdRoutingBean;

	private USSDSessionService ussdSessionService;

	private FarmerService farmerService;

	private EcoFarmerRegistrationHandler ecoFarmerRegistrationHandler;

	@Override
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		ussdSession.setUssdSessionStage(USSDSessionStage.INFORMATION_SERVICES);
		Optional<InformationServicesSessionData> subscriberSessionData = Optional.ofNullable(ussdSession.getInformationServicesSessionData());
		if(subscriberSessionData.isPresent()){
			if(subscriberSessionData.get().getStage().equals(USSDSessionStage.REGISTRATION)){
				String subscriberResponse = request.getMessage().trim();
				if (subscriberResponse.equals(ACCEPT_OPTION)) {
					ussdSession.setNextStage(USSDSessionStage.REGISTRATION);
					ussdSessionService.saveUSSDSession(ussdSession);
					return ecoFarmerRegistrationHandler.apply(request, ussdSession);
				} else if (subscriberResponse.equals(CANCEL_OPTION)) {
					return createMessageResponse(request, CANCEL_MESSAGE, GATEWAY_STAGE_COMPLETE);
				} else {
					return createMessageResponse(request, INVALID_OPTION_MESSAGE, GATEWAY_STAGE_COMPLETE);
				}
			}
		}
		else{
			if (!farmerService.isFullyRegistered(ussdSession.getMsisdn())) {
				InformationServicesSessionData sessionData = new InformationServicesSessionData();
				sessionData.setStage(USSDSessionStage.REGISTRATION);
				ussdSession.setInformationServicesSessionData(sessionData);
				ussdSessionService.saveUSSDSession(ussdSession);
				return createMessageResponse(request,
						"You are not registered on EcoFarmer or your registraion is not complete. Would you like to complete registration now? \n1.Complete Registration \n2.Cancel",
						GATEWAY_STAGE_PENDING);
			}
		}
		Optional<RoutingSessionData> sessionData = Optional.ofNullable(ussdSession.getRoutingSessionData());
		if(!sessionData.isPresent()){
			RoutingSessionData routingSessionData = new RoutingSessionData();
			routingSessionData.setCurrentSequencePosition(0L);
			routingSessionData.setApplicationName(ECOFARMER_TIPS);
			ussdSession.setRoutingSessionData(routingSessionData);
			ussdSessionService.saveUSSDSession(ussdSession);
		}
		return ussdRoutingBean.routeUSSDMessage(request);
	}

	public void setEcoFarmerRegistrationHandler(EcoFarmerRegistrationHandler ecoFarmerRegistrationHandler) {
		this.ecoFarmerRegistrationHandler = ecoFarmerRegistrationHandler;
	}

	public void setUssdRoutingBean(USSDRoutingBean ussdRoutingBean) {
		this.ussdRoutingBean = ussdRoutingBean;
	}

	public void setUssdSessionService(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	public void setFarmerService(FarmerService farmerService) {
		this.farmerService = farmerService;
	}

}
