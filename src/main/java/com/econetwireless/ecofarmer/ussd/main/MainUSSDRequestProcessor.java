package com.econetwireless.ecofarmer.ussd.main;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_FIRST;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_PENDING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.SESSION_EXPIRED;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.TRANSACTION_ID;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.TRANSACTION_TYPE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.USSD_CHANNEL;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDMenuHeaderConstants.WELCOME_HEADER;
import static java.util.stream.Collectors.joining;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

import com.econetwireless.ecofarmer.persistence.model.USSDMenu;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.model.USSDSessionStage;
import com.econetwireless.ecofarmer.persistence.service.USSDMenuService;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.ussd.USSDRequestProcessor;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;

public class MainUSSDRequestProcessor implements USSDRequestProcessor {

	private USSDSessionService ussdSessionService;

	private USSDMenuService ussdMenuService;

	private BiFunction<MessageRequestType, USSDSession, MessageResponseType> transactionHandler;

	public MainUSSDRequestProcessor(USSDSessionService ussdSessionService, USSDMenuService ussdMenuService, BiFunction<MessageRequestType, USSDSession, MessageResponseType> transactionHandler) {
		this.ussdSessionService = ussdSessionService;
		this.ussdMenuService = ussdMenuService;
		this.transactionHandler = transactionHandler;
	}

	@Override
	public MessageResponseType processUSSDRequest(final MessageRequestType ussdRequest) {
		MessageResponseType ussdResponse = new MessageResponseType();
		ussdResponse.setChannel(USSD_CHANNEL);
		ussdResponse.setApplicationTransactionID(TRANSACTION_ID);
		ussdResponse.setTransactionType(TRANSACTION_TYPE);
		ussdResponse.setTransactionTime(ussdRequest.getTransactionTime());
		ussdResponse.setTransactionID(ussdRequest.getTransactionID());
		Optional<USSDSession> subscriberSession = Optional.ofNullable(
						ussdSessionService.findBySessionID(ussdRequest.getTransactionID()));
		if (ussdRequest.getStage().equals(GATEWAY_STAGE_FIRST)) {
			USSDSession ussdSession = new USSDSession();
			ussdSession.setMsisdn(ussdRequest.getSourceNumber());
			ussdSession.setSessionID(ussdRequest.getTransactionID());
			ussdSession.setSessionStartTime(LocalDateTime.now());
			ussdSession.setNextStage(USSDSessionStage.MAIN_MENU);
			ussdSessionService.saveUSSDSession(ussdSession);
			List<USSDMenu> menu = ussdMenuService.findBySessionStage(USSDSessionStage.MAIN_MENU);
			String subscriberDisplayMenu = WELCOME_HEADER + menu.stream().map(
							menuItem -> menuItem.getDisplayNumber() + "." + menuItem.getDisplayName()).collect(
											joining("\n"));
			ussdResponse.setMessage(subscriberDisplayMenu);
			ussdResponse.setStage(GATEWAY_STAGE_PENDING);
		} else {
			if (subscriberSession.isPresent()) {
				ussdResponse = transactionHandler.apply(ussdRequest, subscriberSession.get());
			} else {
				ussdResponse.setMessage(SESSION_EXPIRED);
				ussdResponse.setStage(GATEWAY_STAGE_COMPLETE);
			}
		}
		return ussdResponse;
	}

}
