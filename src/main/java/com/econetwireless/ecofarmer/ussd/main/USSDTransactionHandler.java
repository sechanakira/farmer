package com.econetwireless.ecofarmer.ussd.main;

import java.util.Optional;
import java.util.function.BiFunction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.service.USSDMenuService;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;

public class USSDTransactionHandler implements AbstractUSSDMenuHandler {

	@Autowired
	private ApplicationContext applicationContext;

	private USSDMenuService ussdMenuService;

	private BiFunction<MessageRequestType, USSDSession, MessageResponseType> defaultHandler;

	@Override
	@SuppressWarnings("unchecked")
	public MessageResponseType apply(MessageRequestType request, USSDSession ussdSession) {
		BiFunction<MessageRequestType, USSDSession, MessageResponseType> handler;
		Optional<String> handlerClass = Optional.ofNullable(ussdMenuService.findBySessionStageAndDisplayNumber(
						ussdSession.getUssdSessionStage(), Long.parseLong(request.getMessage())).getHandlerClass());
		if (handlerClass.isPresent()) {
			try {
				handler = (BiFunction<MessageRequestType, USSDSession, MessageResponseType>) applicationContext.getBean(
								Class.forName(handlerClass.get()));
			} catch (ClassNotFoundException ex) {
				handler = defaultHandler;
			}
		} else {
			handler = defaultHandler;
		}
		return handler.apply(request, ussdSession);
	}

	public void setUssdMenuService(USSDMenuService ussdMenuService) {
		this.ussdMenuService = ussdMenuService;
	}

	public void setDefaultHandler(BiFunction<MessageRequestType, USSDSession, MessageResponseType> defaultHandler) {
		this.defaultHandler = defaultHandler;
	}

}
