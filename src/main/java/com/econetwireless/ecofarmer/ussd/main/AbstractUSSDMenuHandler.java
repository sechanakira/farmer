package com.econetwireless.ecofarmer.ussd.main;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.TRANSACTION_ID;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.TRANSACTION_TYPE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.USSD_CHANNEL;

import java.util.function.BiFunction;

import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;

public interface AbstractUSSDMenuHandler extends BiFunction<MessageRequestType, USSDSession, MessageResponseType> {

	public static MessageResponseType createMessageResponse(MessageRequestType request, String message, String stage) {
		MessageResponseType ussdResponse = new MessageResponseType();
		ussdResponse.setChannel(USSD_CHANNEL);
		ussdResponse.setApplicationTransactionID(TRANSACTION_ID);
		ussdResponse.setTransactionType(TRANSACTION_TYPE);
		ussdResponse.setTransactionTime(request.getTransactionTime());
		ussdResponse.setTransactionID(request.getTransactionID());
		ussdResponse.setMessage(message);
		ussdResponse.setStage(stage);
		return ussdResponse;
	}

}
