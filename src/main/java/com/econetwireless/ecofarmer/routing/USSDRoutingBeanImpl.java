package com.econetwireless.ecofarmer.routing;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_COMPLETE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.GATEWAY_STAGE_PENDING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.TRANSACTION_ID;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.TRANSACTION_TYPE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.GatewayConstants.USSD_CHANNEL;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.ECOFARMER_SHORT_CODE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.FIRST_REQUEST_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.FIRST_REQUEST_STAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.METHOD_NAME_HANDLE_REQUEST;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.SDP_EMPTY_STRING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.SDP_FIRST_REQUEST_MESSAGE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.SDP_IMSI;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.SDP_MSISDN;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.SDP_RESPONSE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.SDP_RESPONSE_FALSE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.SDP_SEQUENCE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.SDP_SESSION_ID;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.SDP_TRANSACTION_ID;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.SDP_TRANSACTION_TIME;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.SDP_USSD_REQUEST_STRING;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.SDP_USSD_SERVICE_CODE;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.SDP_VLR;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.USSD_ACTION;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.USSD_END;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.USSDRoutingConstants.USSD_RESPONSE_STRING;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.econetwireless.ecofarmer.persistence.model.RoutingConfig;
import com.econetwireless.ecofarmer.persistence.model.RoutingSessionData;
import com.econetwireless.ecofarmer.persistence.model.RoutingType;
import com.econetwireless.ecofarmer.persistence.model.USSDSession;
import com.econetwireless.ecofarmer.persistence.service.RoutingConfigService;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.sdp.MemberType;
import com.econetwireless.ecofarmer.sdp.MethodCallType;
import com.econetwireless.ecofarmer.sdp.MethodResponseType;
import com.econetwireless.ecofarmer.sdp.ParamType;
import com.econetwireless.ecofarmer.sdp.ParamsType;
import com.econetwireless.ecofarmer.sdp.StructType;
import com.econetwireless.ecofarmer.sdp.ValueType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;

public class USSDRoutingBeanImpl implements USSDRoutingBean {

	private RoutingConfigService routingService;

	private USSDSessionService ussdSessionService;

	private RestTemplate restTemplate;

	@Override
	public MessageResponseType routeUSSDMessage(MessageRequestType messageRequest) {
		RoutingSessionData routingSessionData = ussdSessionService.findBySessionID(
						messageRequest.getTransactionID()).getRoutingSessionData();
		String applicationName = routingSessionData.getApplicationName();
		RoutingConfig routingConfig = routingService.findByApplicationName(applicationName);
		if (routingConfig.getRoutingType().equals(RoutingType.SDP)) {
			return sdpRouting(messageRequest, routingConfig.getRoutingURL());
		}
		if (routingConfig.getRoutingType().equals(RoutingType.IS_GATEWAY)) {
			return internalGatewayRouting(messageRequest, routingConfig.getRoutingURL());
		}
		return null;
	}

	private MessageResponseType internalGatewayRouting(MessageRequestType messageRequest, String url) {
		RoutingSessionData routingSessionData = ussdSessionService.findBySessionID(
						messageRequest.getTransactionID()).getRoutingSessionData();
		if (routingSessionData.getCurrentSequencePosition().equals(0L)) {
			messageRequest.setMessage(FIRST_REQUEST_MESSAGE);
			messageRequest.setStage(FIRST_REQUEST_STAGE);
		}
		try {
			ResponseEntity<MessageResponseType> response = restTemplate.postForEntity(new URI(url), messageRequest,
							MessageResponseType.class);
			incrementSequence(messageRequest);
			return response.getBody();
		} catch (Exception ex) {
		}
		return null;
	}

	private MessageResponseType sdpRouting(MessageRequestType messageRequest, String url) {
		RoutingSessionData routingSessionData = ussdSessionService.findBySessionID(
						messageRequest.getTransactionID()).getRoutingSessionData();
		if (routingSessionData.getCurrentSequencePosition().equals(0L)) {
			messageRequest.setMessage(SDP_FIRST_REQUEST_MESSAGE);
		}
		MethodCallType methodCallType = new MethodCallType();
		methodCallType.setMethodName(METHOD_NAME_HANDLE_REQUEST);
		ParamsType paramsType = new ParamsType();
		ParamType paramType = new ParamType();
		ValueType valueType = new ValueType();
		valueType.setStruct(setMembers(messageRequest));
		paramType.setValue(valueType);
		paramsType.setParam(paramType);
		methodCallType.setParams(paramsType);
		return createSDPUSSDResponse(messageRequest, sendSDPRequest(methodCallType, url));
	}

	private StructType setMembers(MessageRequestType ussdRequest) {
		StructType structType = new StructType();
		structType.getMember().add(0, createSDPTransIDMemberType(ussdRequest));
		structType.getMember().add(1, createSDPDateTimeMemberType());
		structType.getMember().add(2, createSDPMSISDNMemberType(ussdRequest));
		structType.getMember().add(3, createShortCodeMemberType());
		structType.getMember().add(4, createUSSDStringMemberType(ussdRequest));
		structType.getMember().add(5, createSDPResponseMemberType());
		structType.getMember().add(6, createSDPIMSIMemberType());
		structType.getMember().add(7, createSDPSequenceMemberType(ussdRequest));
		structType.getMember().add(8, createSDPSessionIdmemberType(ussdRequest));
		structType.getMember().add(9, createSDPVLRMemberType());
		return structType;
	}

	private MemberType createSDPTransIDMemberType(MessageRequestType ussdRequest) {
		ValueType sdpTransIDvalueType = new ValueType();
		MemberType sdpTransIDmemberType = new MemberType();
		sdpTransIDvalueType.setString(ussdRequest.getTransactionID());
		sdpTransIDmemberType.setName(SDP_TRANSACTION_ID);
		sdpTransIDmemberType.setValue(sdpTransIDvalueType);
		return sdpTransIDmemberType;
	}

	private MemberType createSDPDateTimeMemberType() {
		ValueType sdpDateTimevalueType = new ValueType();
		MemberType sdpDateTimememberType = new MemberType();
		sdpDateTimevalueType.setDateTimeIso8601(createSDPISODate());
		sdpDateTimememberType.setName(SDP_TRANSACTION_TIME);
		sdpDateTimememberType.setValue(sdpDateTimevalueType);
		return sdpDateTimememberType;
	}

	private MemberType createSDPMSISDNMemberType(MessageRequestType ussdRequest) {
		ValueType sdpMSISDNvalueType = new ValueType();
		MemberType sdpMSISDNmemberType = new MemberType();
		sdpMSISDNvalueType.setString(ussdRequest.getSourceNumber());
		sdpMSISDNmemberType.setName(SDP_MSISDN);
		sdpMSISDNmemberType.setValue(sdpMSISDNvalueType);
		return sdpMSISDNmemberType;
	}

	private MemberType createShortCodeMemberType() {
		ValueType shortCodevalueType = new ValueType();
		MemberType shortCodememberType = new MemberType();
		shortCodevalueType.setString(ECOFARMER_SHORT_CODE);
		shortCodememberType.setName(SDP_USSD_SERVICE_CODE);
		shortCodememberType.setValue(shortCodevalueType);
		return shortCodememberType;
	}

	private MemberType createUSSDStringMemberType(MessageRequestType ussdRequest) {
		ValueType ussdStringvalueType = new ValueType();
		MemberType ussdStringmemberType = new MemberType();
		ussdStringvalueType.setString(ussdRequest.getMessage().trim());
		ussdStringmemberType.setName(SDP_USSD_REQUEST_STRING);
		ussdStringmemberType.setValue(ussdStringvalueType);
		return ussdStringmemberType;
	}

	private MemberType createSDPResponseMemberType() {
		ValueType sdpResponsevalueType = new ValueType();
		MemberType sdpResponsememberType = new MemberType();
		sdpResponsevalueType.setString(SDP_RESPONSE_FALSE);
		sdpResponsememberType.setName(SDP_RESPONSE);
		sdpResponsememberType.setValue(sdpResponsevalueType);
		return sdpResponsememberType;
	}

	private MemberType createSDPIMSIMemberType() {
		ValueType sdpIMSIvalueType = new ValueType();
		MemberType sdpIMSImemberType = new MemberType();
		sdpIMSIvalueType.setString(SDP_EMPTY_STRING);
		sdpIMSImemberType.setName(SDP_IMSI);
		sdpIMSImemberType.setValue(sdpIMSIvalueType);
		return sdpIMSImemberType;
	}

	private MemberType createSDPSequenceMemberType(MessageRequestType ussdRequest) {
		USSDSession subscriberSession = ussdSessionService.findBySessionID(ussdRequest.getTransactionID());
		ValueType sdpSequencevalueType = new ValueType();
		MemberType sdpSequencememberType = new MemberType();
		incrementSequence(ussdRequest);
		sdpSequencevalueType.setInt(subscriberSession.getRoutingSessionData().getCurrentSequencePosition() + "");
		sdpSequencememberType.setName(SDP_SEQUENCE);
		sdpSequencememberType.setValue(sdpSequencevalueType);
		return sdpSequencememberType;
	}

	private MemberType createSDPSessionIdmemberType(MessageRequestType ussdRequest) {
		ValueType sdpSessionIdvalueType = new ValueType();
		MemberType sdpSessionIdmemberType = new MemberType();
		sdpSessionIdvalueType.setString(ussdRequest.getTransactionID());
		sdpSessionIdmemberType.setName(SDP_SESSION_ID);
		sdpSessionIdmemberType.setValue(sdpSessionIdvalueType);
		return sdpSessionIdmemberType;
	}

	private MemberType createSDPVLRMemberType() {
		ValueType sdpVLRvalueType = new ValueType();
		MemberType sdpVLRmemberType = new MemberType();
		sdpVLRvalueType.setString(SDP_EMPTY_STRING);
		sdpVLRmemberType.setName(SDP_VLR);
		sdpVLRmemberType.setValue(sdpVLRvalueType);
		return sdpVLRmemberType;
	}

	private MessageResponseType createSDPUSSDResponse(MessageRequestType ussdRequest, MethodResponseType methodResponseType) {
		MessageResponseType ussdResponse = new MessageResponseType();
		ussdResponse.setTransactionTime(LocalDateTime.now());
		ussdResponse.setTransactionID(ussdRequest.getTransactionID());
		ussdResponse.setSourceNumber(ussdRequest.getSourceNumber());
		ussdResponse.setDestinationNumber(ussdRequest.getDestinationNumber());
		ussdResponse.setStage(GATEWAY_STAGE_PENDING);
		ussdResponse.setChannel(USSD_CHANNEL);
		ussdResponse.setApplicationTransactionID(TRANSACTION_ID);
		ussdResponse.setTransactionType(TRANSACTION_TYPE);
		List<MemberType> members = methodResponseType.getParams().getParam().getValue().getStruct().getMember();
		for (MemberType memberType : members) {
			if (memberType.getName().equals(USSD_RESPONSE_STRING)) {
				ussdResponse.setMessage(memberType.getValue().getString());
			}
			if (memberType.getName().equals(USSD_ACTION) && memberType.getValue().getString().equals(USSD_END)) {
				ussdResponse.setStage(GATEWAY_STAGE_COMPLETE);
			}
		}
		return ussdResponse;
	}

	private MethodResponseType sendSDPRequest(MethodCallType methodCallType, String url) {
		try {
			ResponseEntity<MethodResponseType> response = restTemplate.postForEntity(new URI(url), methodCallType,
							MethodResponseType.class);
			return response.getBody();
		} catch (Exception ex) {
		}
		return null;
	}

	private void incrementSequence(MessageRequestType messageRequest) {
		USSDSession subscriberSession = ussdSessionService.findBySessionID(messageRequest.getTransactionID());
		RoutingSessionData routingSessionData = ussdSessionService.findBySessionID(
						messageRequest.getTransactionID()).getRoutingSessionData();
		routingSessionData.setCurrentSequencePosition(routingSessionData.getCurrentSequencePosition() + 1l);
		subscriberSession.setRoutingSessionData(routingSessionData);
		ussdSessionService.saveUSSDSession(subscriberSession);
	}

	private String createSDPISODate(){
		LocalDateTime date = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd'T'HH:mm:ss");
		return date.format(formatter);
	}

	public void setRoutingService(RoutingConfigService routingService) {
		this.routingService = routingService;
	}

	public void setUssdSessionService(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

}
