package com.econetwireless.ecofarmer.routing;

import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;

public interface USSDRoutingBean {

	MessageResponseType routeUSSDMessage(MessageRequestType messageRequestType);
}
