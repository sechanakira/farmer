
package com.econetwireless.ecofarmer.sdp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for valueType complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="valueType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="string" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="00001"/>
 *               &lt;enumeration value="275551234"/>
 *               &lt;enumeration value="543"/>
 *               &lt;enumeration value="Subscriber response message"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dateTime.iso8601" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="int" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="struct" type="{}structType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "valueType", propOrder = { "string", "dateTimeIso8601", "_int", "struct" })
public class ValueType {

	protected String string;
	@XmlElement(name = "dateTime.iso8601")
	protected String dateTimeIso8601;
	@XmlElement(name = "int")
	protected String _int;
	protected StructType struct;

	/**
	 * Gets the value of the string property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getString() {
		return string;
	}

	/**
	 * Sets the value of the string property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setString(String value) {
		this.string = value;
	}

	/**
	 * Gets the value of the dateTimeIso8601 property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getDateTimeIso8601() {
		return dateTimeIso8601;
	}

	/**
	 * Sets the value of the dateTimeIso8601 property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setDateTimeIso8601(String value) {
		this.dateTimeIso8601 = value;
	}

	/**
	 * Gets the value of the int property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getInt() {
		return _int;
	}

	/**
	 * Sets the value of the int property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setInt(String value) {
		this._int = value;
	}

	/**
	 * Gets the value of the struct property.
	 * 
	 * @return possible object is {@link StructType }
	 */
	public StructType getStruct() {
		return struct;
	}

	/**
	 * Sets the value of the struct property.
	 * 
	 * @param value
	 *            allowed object is {@link StructType }
	 */
	public void setStruct(StructType value) {
		this.struct = value;
	}

}
