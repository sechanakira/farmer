
package com.econetwireless.ecofarmer.sdp;

import javax.xml.bind.annotation.*;

/**
 * <p>
 * Java class for methodCallType complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="methodCallType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="methodName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="params" type="{}paramsType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlRootElement(name = "methodCall")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "methodCallType", propOrder = { "methodName", "params" })
public class MethodCallType {

	@XmlElement(required = true)
	protected String methodName;
	@XmlElement(required = true)
	protected ParamsType params;

	/**
	 * Gets the value of the methodName property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * Sets the value of the methodName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setMethodName(String value) {
		this.methodName = value;
	}

	/**
	 * Gets the value of the params property.
	 * 
	 * @return possible object is {@link ParamsType }
	 */
	public ParamsType getParams() {
		return params;
	}

	/**
	 * Sets the value of the params property.
	 * 
	 * @param value
	 *            allowed object is {@link ParamsType }
	 */
	public void setParams(ParamsType value) {
		this.params = value;
	}

}
