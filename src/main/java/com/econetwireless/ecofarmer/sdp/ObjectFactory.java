
package com.econetwireless.ecofarmer.sdp;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the zw.co.econet.ecofarmermenu.hxc package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

	private final static QName _MethodResponse_QNAME = new QName("", "methodResponse");
	private final static QName _MethodCall_QNAME = new QName("", "methodCall");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: zw.co.econet.ecofarmermenu.hxc
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link MethodResponseType }
	 */
	public MethodResponseType createMethodResponseType() {
		return new MethodResponseType();
	}

	/**
	 * Create an instance of {@link ParamType }
	 */
	public ParamType createParamType() {
		return new ParamType();
	}

	/**
	 * Create an instance of {@link StructType }
	 */
	public StructType createStructType() {
		return new StructType();
	}

	/**
	 * Create an instance of {@link ValueType }
	 */
	public ValueType createValueType() {
		return new ValueType();
	}

	/**
	 * Create an instance of {@link MemberType }
	 */
	public MemberType createMemberType() {
		return new MemberType();
	}

	/**
	 * Create an instance of {@link ParamsType }
	 */
	public ParamsType createParamsType() {
		return new ParamsType();
	}

	/**
	 * Create an instance of {@link JAXBElement
	 * }{@code <}{@link MethodResponseType }{@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "methodResponse")
	public JAXBElement<MethodResponseType> createMethodResponse(MethodResponseType value) {
		return new JAXBElement<MethodResponseType>(_MethodResponse_QNAME, MethodResponseType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link MethodCallType
	 * }{@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "methodCall")
	public JAXBElement<MethodCallType> createMethodCall(MethodCallType value) {
		return new JAXBElement<MethodCallType>(_MethodCall_QNAME, MethodCallType.class, null, value);
	}

}
