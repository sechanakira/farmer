
package com.econetwireless.ecofarmer.sdp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for paramsType complex type.
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="paramsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="param" type="{}paramType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "paramsType", propOrder = { "param" })
public class ParamsType {

	@XmlElement(required = true)
	protected ParamType param;

	/**
	 * Gets the value of the param property.
	 * 
	 * @return possible object is {@link ParamType }
	 */
	public ParamType getParam() {
		return param;
	}

	/**
	 * Sets the value of the param property.
	 * 
	 * @param value
	 *            allowed object is {@link ParamType }
	 */
	public void setParam(ParamType value) {
		this.param = value;
	}

	@Override
	public String toString() {
		return "ParamsType{" +
				"param=" + param +
				'}';
	}
}
