package com.econetwireless.ecofarmer.controller.advice;

/**
 * Created by Shingirai.Chanakira on 2/19/2017.
 */
public class UserNotFoundErrorMessage {

    private String message;

    public UserNotFoundErrorMessage(String message){
        this.message = message;
    }

}
