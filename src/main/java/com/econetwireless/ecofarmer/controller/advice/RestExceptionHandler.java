package com.econetwireless.ecofarmer.controller.advice;

import com.econetwireless.ecofarmer.exception.UserNotRegisteredException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Shingirai.Chanakira on 2/19/2017.
 */
@RestController
@ControllerAdvice
public class RestExceptionHandler {

    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = UserNotRegisteredException.class)
    public UserNotFoundErrorMessage handleUserNotFoundException(UserNotRegisteredException unre){
        return new UserNotFoundErrorMessage(unre.getMessage());
    }
}
