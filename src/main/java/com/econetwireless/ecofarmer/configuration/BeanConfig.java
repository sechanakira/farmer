package com.econetwireless.ecofarmer.configuration;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EmailConstants.SMTP_HOST;

import java.util.ArrayList;
import java.util.List;

import com.econetwireless.ecofarmer.ussd.clubs.EcoFarmerClubsHandler;
import com.econetwireless.ecofarmer.ussd.clubs.ZFUComboHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.client.RestTemplate;

import com.econetwireless.ecofarmer.email.EmailBean;
import com.econetwireless.ecofarmer.email.EmailBeanImpl;
import com.econetwireless.ecofarmer.esoko.EsokoAPI;
import com.econetwireless.ecofarmer.esoko.EsokoAPIImpl;
import com.econetwireless.ecofarmer.persistence.mapping.EcoCashDataRowMapper;
import com.econetwireless.ecofarmer.persistence.service.APIKeyService;
import com.econetwireless.ecofarmer.persistence.service.APIKeyServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.DistrictService;
import com.econetwireless.ecofarmer.persistence.service.DistrictServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.EcoCashCustomerDataService;
import com.econetwireless.ecofarmer.persistence.service.EcoCashCustomerDataServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.EnglishHelpService;
import com.econetwireless.ecofarmer.persistence.service.EnglishHelpServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.EsokoAPICallResponseService;
import com.econetwireless.ecofarmer.persistence.service.EsokoAPICallResponseServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.EsokoBillingOptionService;
import com.econetwireless.ecofarmer.persistence.service.EsokoBillingOptionServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.EsokoPaymentMethodService;
import com.econetwireless.ecofarmer.persistence.service.EsokoPaymentMethodServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.EsokoTipService;
import com.econetwireless.ecofarmer.persistence.service.EsokoTipServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.FarmTypeService;
import com.econetwireless.ecofarmer.persistence.service.FarmTypeServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.FarmerService;
import com.econetwireless.ecofarmer.persistence.service.FarmerServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.LanguageService;
import com.econetwireless.ecofarmer.persistence.service.LanguageServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.NdebeleHelpService;
import com.econetwireless.ecofarmer.persistence.service.NdebeleHelpServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.ProvinceService;
import com.econetwireless.ecofarmer.persistence.service.ProvinceServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.RoutingConfigService;
import com.econetwireless.ecofarmer.persistence.service.RoutingConfigServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.ShonaHelpService;
import com.econetwireless.ecofarmer.persistence.service.ShonaHelpServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.USSDMenuService;
import com.econetwireless.ecofarmer.persistence.service.USSDMenuServiceImpl;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;
import com.econetwireless.ecofarmer.persistence.service.USSDSessionServiceImpl;
import com.econetwireless.ecofarmer.routing.USSDRoutingBean;
import com.econetwireless.ecofarmer.routing.USSDRoutingBeanImpl;
import com.econetwireless.ecofarmer.schedule.task.USSDSessionScheduledTasks;
import com.econetwireless.ecofarmer.sms.SMSBean;
import com.econetwireless.ecofarmer.sms.SMSBeanImpl;
import com.econetwireless.ecofarmer.ussd.bundles.BuyBundlesHandler;
import com.econetwireless.ecofarmer.ussd.clubs.EsokoHandler;
import com.econetwireless.ecofarmer.ussd.ecofarmerservices.EcoFarmerServicesHandler;
import com.econetwireless.ecofarmer.ussd.help.EcoFarmerHelpHandler;
import com.econetwireless.ecofarmer.ussd.infoservices.InformationServicesHandler;
import com.econetwireless.ecofarmer.ussd.language.LanguageChangeHandler;
import com.econetwireless.ecofarmer.ussd.main.DefaultUSSDMenuHandler;
import com.econetwireless.ecofarmer.ussd.main.MainUSSDRequestProcessor;
import com.econetwireless.ecofarmer.ussd.main.USSDTransactionHandler;
import com.econetwireless.ecofarmer.ussd.registration.DistrictSelectionHandler;
import com.econetwireless.ecofarmer.ussd.registration.EcoFarmerRegistrationHandler;
import com.econetwireless.ecofarmer.ussd.registration.EcocashDataHandler;
import com.econetwireless.ecofarmer.ussd.registration.FarmTypeSelectionHandler;
import com.econetwireless.ecofarmer.ussd.registration.LanguageSelectionHandler;
import com.econetwireless.ecofarmer.ussd.registration.ProvinceSelectionHandler;
import com.econetwireless.ecofarmer.ussd.registration.ViewRegistrationDetailsHandler;
import com.econetwireless.ecofarmer.ussd.registration.WardSelectionHandler;
import com.econetwireless.ecofarmer.zfu.EcosureAPI;
import com.econetwireless.ecofarmer.zfu.EcosureAPIImpl;

@Configuration
public class BeanConfig {

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public SMSBean smsBean() {
		return new SMSBeanImpl(restTemplate());
	}

	@Bean
	public MailSender mailSender() {
		JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();
		javaMailSenderImpl.setHost(SMTP_HOST);
		return javaMailSenderImpl;
	}

	@Bean
	public EmailBean emailBean() {
		return new EmailBeanImpl(mailSender());
	}

	@Bean
	public DistrictService districtService() {
		return new DistrictServiceImpl();
	}

	@Bean
	public ProvinceService provinceService() {
		return new ProvinceServiceImpl();
	}

	@Bean
	public EcoCashCustomerDataService ecoCashCustomerDataService() {
		return new EcoCashCustomerDataServiceImpl(ecoCashDataRowMapper());
	}

	@Bean
	public USSDMenuService ussdMenuService() {
		return new USSDMenuServiceImpl();
	}

	@Bean
	public USSDSessionService ussdSessionService() {
		return new USSDSessionServiceImpl();
	}

	@Bean
	public FarmTypeService farmTypeService() {
		return new FarmTypeServiceImpl();
	}

	@Bean
	public LanguageService languageService() {
		return new LanguageServiceImpl();
	}

	@Bean
	public RoutingConfigService routingConfigService() {
		return new RoutingConfigServiceImpl();
	}

	@Bean
	public USSDTransactionHandler ussdTransactionHandler() {
		USSDTransactionHandler ussdTransactionHandler = new USSDTransactionHandler();
		ussdTransactionHandler.setDefaultHandler(defaultUSSDMenuHandler());
		ussdTransactionHandler.setUssdMenuService(ussdMenuService());
		return ussdTransactionHandler;
	}

	@Bean
	public MainUSSDRequestProcessor mainUSSDRequestProcessor() {
		return new MainUSSDRequestProcessor(ussdSessionService(), ussdMenuService(), ussdTransactionHandler());
	}

	@Bean
	public USSDSessionScheduledTasks ussdSessionScheduledTasks() {
		return new USSDSessionScheduledTasks(ussdSessionService());
	}

	@Bean
	public DefaultUSSDMenuHandler defaultUSSDMenuHandler() {
		return new DefaultUSSDMenuHandler();
	}

	@Bean
	public FarmerService farmerService() {
		return new FarmerServiceImpl();
	}

	@Bean
	public EcoFarmerRegistrationHandler ecoFarmerRegistrationHandler() {
		EcoFarmerRegistrationHandler ecoFarmerRegistrationHandler = new EcoFarmerRegistrationHandler();
		ecoFarmerRegistrationHandler.setFarmerService(farmerService());
		ecoFarmerRegistrationHandler.setEcoCashCustomerDataService(ecoCashCustomerDataService());
		ecoFarmerRegistrationHandler.setEcoCashCustomerDataHandler(ecocashDataHandler());
		ecoFarmerRegistrationHandler.setUssdSessionService(ussdSessionService());
		ecoFarmerRegistrationHandler.setProvinceSelectionHandler(provinceSelectionHandler());
		ecoFarmerRegistrationHandler.setFarmTypeSelectionHandler(farmTypeSelectionHandler());
		ecoFarmerRegistrationHandler.setLanguageSelectionHandler(languageSelectionHandler());
		ecoFarmerRegistrationHandler.setDistrictSelectionHandler(districtSelectionHandler());
		ecoFarmerRegistrationHandler.setWardSelectionHandler(wardSelectionHandler());
		ecoFarmerRegistrationHandler.setViewRegistrationDetailsHandler(viewRegistrationDetailsHandler());
		return ecoFarmerRegistrationHandler;
	}

	@Bean
	public EcoCashDataRowMapper ecoCashDataRowMapper() {
		return new EcoCashDataRowMapper();
	}

	@Bean
	public EcocashDataHandler ecocashDataHandler() {
		EcocashDataHandler ecoCashDataHandler = new EcocashDataHandler();
		ecoCashDataHandler.setEcoCashCustomerDataService(ecoCashCustomerDataService());
		ecoCashDataHandler.setUssdSessionService(ussdSessionService());
		ecoCashDataHandler.setSuccessor(provinceSelectionHandler());
		return ecoCashDataHandler;
	}

	@Bean
	public ProvinceSelectionHandler provinceSelectionHandler() {
		ProvinceSelectionHandler provinceSelectionHandler = new ProvinceSelectionHandler();
		provinceSelectionHandler.setFarmerService(farmerService());
		provinceSelectionHandler.setProvinceService(provinceService());
		provinceSelectionHandler.setSuccessor(districtSelectionHandler());
		provinceSelectionHandler.setUssdSessionService(ussdSessionService());
		return provinceSelectionHandler;
	}

	@Bean
	public DistrictSelectionHandler districtSelectionHandler() {
		DistrictSelectionHandler districtSelectionHandler = new DistrictSelectionHandler();
		districtSelectionHandler.setDistrictService(districtService());
		districtSelectionHandler.setFarmerService(farmerService());
		districtSelectionHandler.setSuccessor(wardSelectionHandler());
		districtSelectionHandler.setUssdSessionService(ussdSessionService());
		return districtSelectionHandler;
	}

	@Bean
	public WardSelectionHandler wardSelectionHandler() {
		WardSelectionHandler wardSelectionHandler = new WardSelectionHandler();
		wardSelectionHandler.setDistrictService(districtService());
		wardSelectionHandler.setFarmerService(farmerService());
		wardSelectionHandler.setSuccessor(farmTypeSelectionHandler());
		wardSelectionHandler.setUssdSessionService(ussdSessionService());
		return wardSelectionHandler;
	}

	@Bean
	public FarmTypeSelectionHandler farmTypeSelectionHandler() {
		FarmTypeSelectionHandler farmTypeSelectionHandler = new FarmTypeSelectionHandler();
		farmTypeSelectionHandler.setFarmerService(farmerService());
		farmTypeSelectionHandler.setFarmTypeService(farmTypeService());
		farmTypeSelectionHandler.setSuccessor(languageSelectionHandler());
		farmTypeSelectionHandler.setUssdSessionService(ussdSessionService());
		return farmTypeSelectionHandler;
	}

	@Bean
	public LanguageSelectionHandler languageSelectionHandler() {
		LanguageSelectionHandler languageSelectionHandler = new LanguageSelectionHandler();
		languageSelectionHandler.setFarmerService(farmerService());
		languageSelectionHandler.setLanguageService(languageService());
		languageSelectionHandler.setSmsBean(smsBean());
		languageSelectionHandler.setUssdSessionService(ussdSessionService());
		return languageSelectionHandler;
	}

	@Bean
	public ViewRegistrationDetailsHandler viewRegistrationDetailsHandler() {
		ViewRegistrationDetailsHandler viewRegistrationDetailsHandler = new ViewRegistrationDetailsHandler();
		viewRegistrationDetailsHandler.setProvinceService(provinceService());
		viewRegistrationDetailsHandler.setDistrictService(districtService());
		viewRegistrationDetailsHandler.setFarmerService(farmerService());
		viewRegistrationDetailsHandler.setLanguageService(languageService());
		return viewRegistrationDetailsHandler;
	}

	@Bean
	public LanguageChangeHandler languageChangeHandler() {
		LanguageChangeHandler languageChangeHandler = new LanguageChangeHandler();
		languageChangeHandler.setEcoFarmerRegistrationHandler(ecoFarmerRegistrationHandler());
		languageChangeHandler.setFarmerService(farmerService());
		languageChangeHandler.setLanguageService(languageService());
		languageChangeHandler.setUssdSessionService(ussdSessionService());
		return languageChangeHandler;
	}

	@Bean
	public EcoFarmerServicesHandler ecoFarmerServicesHandler() {
		EcoFarmerServicesHandler ecoFarmerServicesHandler = new EcoFarmerServicesHandler();
		ecoFarmerServicesHandler.setUssdRoutingBean(ussdRoutingBean());
		ecoFarmerServicesHandler.setUssdSessionService(ussdSessionService());
		return ecoFarmerServicesHandler;
	}

	@Bean
	public InformationServicesHandler informationServicesHandler() {
		InformationServicesHandler informationServicesHandler = new InformationServicesHandler();
		informationServicesHandler.setFarmerService(farmerService());
		informationServicesHandler.setUssdRoutingBean(ussdRoutingBean());
		informationServicesHandler.setUssdSessionService(ussdSessionService());
		informationServicesHandler.setEcoFarmerRegistrationHandler(ecoFarmerRegistrationHandler());
		return informationServicesHandler;
	}

	@Bean
	public BuyBundlesHandler buyBundlesHandler() {
		return new BuyBundlesHandler();
	}

	@Bean
	public USSDRoutingBean ussdRoutingBean() {
		USSDRoutingBeanImpl ussdRoutingBeanImpl = new USSDRoutingBeanImpl();
		ussdRoutingBeanImpl.setRestTemplate(restTemplate());
		ussdRoutingBeanImpl.setRoutingService(routingConfigService());
		ussdRoutingBeanImpl.setUssdSessionService(ussdSessionService());
		return ussdRoutingBeanImpl;
	}

	@Bean
	public EnglishHelpService englishHelpService() {
		return new EnglishHelpServiceImpl();
	}

	@Bean
	public ShonaHelpService shonaHelpService() {
		return new ShonaHelpServiceImpl();
	}

	@Bean
	public NdebeleHelpService ndebeleHelpService() {
		return new NdebeleHelpServiceImpl();
	}

	@Bean
	public APIKeyService apiKeyService() {
		return new APIKeyServiceImpl();
	}

	@Bean
	public EcoFarmerHelpHandler ecoFarmerHelpHandler() {
		EcoFarmerHelpHandler ecofarmerHelpHandler = new EcoFarmerHelpHandler();
		ecofarmerHelpHandler.setFarmerService(farmerService());
		ecofarmerHelpHandler.setEnglishHelpService(englishHelpService());
		ecofarmerHelpHandler.setNdebeleHelpService(ndebeleHelpService());
		ecofarmerHelpHandler.setShonaHelpService(shonaHelpService());
		ecofarmerHelpHandler.setUssdSessionService(ussdSessionService());
		return ecofarmerHelpHandler;
	}

	@Bean
	public EcosureAPI ecosureAPI() {
		EcosureAPIImpl ecosureAPIImpl = new EcosureAPIImpl();
		ecosureAPIImpl.setRestTemplate(restTemplate());
		return ecosureAPIImpl;
	}

	@Bean
	public EsokoAPICallResponseService esokoAPICallResponseService() {
		return new EsokoAPICallResponseServiceImpl();
	}

	@Bean
	public EsokoAPI esokoAPI() {
		RestTemplate restTemplate = new RestTemplate();
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
		messageConverters.add(new FormHttpMessageConverter());
		messageConverters.add(new StringHttpMessageConverter());
		messageConverters.add(new Jaxb2RootElementHttpMessageConverter());
		restTemplate.setMessageConverters(messageConverters);
		EsokoAPIImpl esokoAPIIMpl = new EsokoAPIImpl();
		esokoAPIIMpl.setApiKeyService(apiKeyService());
		esokoAPIIMpl.setEsokoAPICallResponseService(esokoAPICallResponseService());
		esokoAPIIMpl.setRestTemplate(restTemplate);
		return esokoAPIIMpl;
	}
	
	@Bean
	public EsokoTipService esokoTipService(){
		return new EsokoTipServiceImpl();
	}
	
	@Bean
	public EsokoBillingOptionService esokoBillingOptionService(){
		return new EsokoBillingOptionServiceImpl();
	}
	
	@Bean
	public EsokoPaymentMethodService esokoPaymentMethodService(){
		return new EsokoPaymentMethodServiceImpl();
	}
	
	@Bean
	public EsokoHandler esokoHandler(){
		EsokoHandler esokoHandler = new EsokoHandler();
		esokoHandler.setEcofarmerRegistrationHandler(ecoFarmerRegistrationHandler());
		esokoHandler.setFarmerService(farmerService());
		esokoHandler.setUssdSessionService(ussdSessionService());
		esokoHandler.setEsokoHelpHandler(null);
		esokoHandler.setEsokoOfferToSellHandler(null);
		esokoHandler.setEsokoUnsubscribeHandler(null);
		return esokoHandler;
	}

	@Bean
	public EcoFarmerClubsHandler ecoFarmerClubsHandler(){
		EcoFarmerClubsHandler ecoFarmerClubsHandler = new EcoFarmerClubsHandler();
		ecoFarmerClubsHandler.setEsokoHandler(esokoHandler());
		ecoFarmerClubsHandler.setUssdSessionService(ussdSessionService());
		ecoFarmerClubsHandler.setZfuComboHandler(zfuComboHandler());
		return ecoFarmerClubsHandler;
	}

	@Bean
	public ZFUComboHandler zfuComboHandler(){
		return new ZFUComboHandler();
	}
}
