package com.econetwireless.ecofarmer.zfu;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.ZFUConstants.ECOSURE_AUTH_URL;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.ZFUConstants.ECOSURE_PRODUCTS_URL;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.ZFUConstants.ECOSURE_REGISTER;
import static com.econetwireless.ecofarmer.constants.ApplicationConstants.ZFUConstants.ECOSURE_REG_URL;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class EcosureAPIImpl implements EcosureAPI {

	private RestTemplate restTemplate;

	@Override
	public AuthenticationRequest checkAuth(AuthenticationRequest authenticationRequest) {
		return (AuthenticationRequest) buildAndSendRequest(authenticationRequest, ECOSURE_AUTH_URL).getBody();
	}

	@Override
	public AuthenticationRequest checkReg(AuthenticationRequest authenticationRequest) {
		return (AuthenticationRequest) buildAndSendRequest(authenticationRequest, ECOSURE_REG_URL).getBody();
	}

	@Override
	public ProductInfoList getProducts(AuthenticationRequest authenticationRequest) {
		return (ProductInfoList) buildAndSendRequest(authenticationRequest, ECOSURE_PRODUCTS_URL).getBody();
	}

	@Override
	public SubscriberInfo createEcoSureSubcription(SubscriberInfo subscriberInfo) {
		return (SubscriberInfo) buildAndSendRequest(subscriberInfo, ECOSURE_REGISTER).getBody();
	}

	@Override
	public boolean checkZFUREg(AuthenticationRequest authenticationRequest) {
		AuthenticationRequest authenticationResponse = (AuthenticationRequest) buildAndSendRequest(
						authenticationRequest, ECOSURE_REG_URL).getBody();
		return Boolean.parseBoolean(authenticationResponse.getData());
	}

	@Override
	public AddOnRequest enableZFUAddOn(AddOnRequest addOnRequest) {
		return null;
	}

	@Override
	public AddOnRequest disableZFUAddOn(AddOnRequest addOnRequest) {
		return null;
	}

	@Override
	public AddOnRequest hasAddOn(AddOnRequest addOnRequest) {
		return null;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@SuppressWarnings("rawtypes")
	private <T> ResponseEntity buildAndSendRequest(T request, String url) {
		return restTemplate.postForEntity(url, request, Object.class);
	}
}
