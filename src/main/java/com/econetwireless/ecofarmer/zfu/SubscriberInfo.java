package com.econetwireless.ecofarmer.zfu;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SubscriberInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String firstName;
	private String surname;
	private String msisdn;
	private String dateOfBirth;
	private String gender;
	private String address;
	private String packageName;
	private String nationalId;
	private String merchantCode;
	private String responseCode;
	private String responseMessage;
	private String subscription;
	private String countryCode;
	private String beneficiaryCode;
	private String beneficiaryType;
	private boolean autoDebit;
	private String acquirer;
	private String companyId;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		if (nationalId != null) {
			nationalId.replaceAll("-", "");
			nationalId.replaceAll(" ", "");
		}
		this.nationalId = nationalId.toUpperCase();
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGender() {
		return gender == null ? "-" : gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getDateOfBirth() {
		return dateOfBirth == null ? "-" : dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getSubscription() {
		return subscription;
	}

	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getBeneficiaryCode() {
		return beneficiaryCode;
	}

	public void setBeneficiaryCode(String beneficiaryCode) {
		this.beneficiaryCode = beneficiaryCode;
	}

	public String getBeneficiaryType() {
		return beneficiaryType;
	}

	public void setBeneficiaryType(String beneficiaryType) {
		this.beneficiaryType = beneficiaryType;
	}

	@Override
	public String toString() {
		return "SubscriberInfo [firstName=" + firstName + ", surname=" + surname + ", msisdn=" + msisdn + ", dateOfBirth=" + dateOfBirth + ", gender=" + gender + ", address=" + address + ", packageName=" + packageName + ", nationalId=" + nationalId + ", merchantCode=" + merchantCode + ", subscription=" + subscription + ", countryCode=" + countryCode + ", beneficiaryCode=" + beneficiaryCode + ", beneficiaryType=" + beneficiaryType + ", autoDebit=" + autoDebit + ", acquirer=" + acquirer + ", responseCode=" + responseCode + ", responseMessage=" + responseMessage + "]";
	}

	public boolean isAutoDebit() {
		return autoDebit;
	}

	public void setAutoDebit(boolean autoDebit) {
		this.autoDebit = autoDebit;
	}

	public String getAcquirer() {
		/*
		 * if (acquirer != null) { return acquirer.replaceAll("[\\W_]+",
		 * "").toUpperCase(); }
		 */
		return acquirer;
	}

	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}

}
