package com.econetwireless.ecofarmer.zfu;

import java.text.DecimalFormat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductInfo {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 8308207676909817366L;

	private String productId;
	private String productName;
	private String displayName;
	private String responseCode;
	private String responseMessage;
	private String premiumAmount;
	private String sumAssured;
	private Integer rating;

	public ProductInfo(String productId, String productName, Integer rating, Double sumAssured, Double premiumPerMember) {
		super();
		this.productId = productId;
		this.productName = productName;
		DecimalFormat moneyFormat = new DecimalFormat("#,##0.00");
		this.sumAssured = "$" + moneyFormat.format(sumAssured);
		this.displayName = productName + " $" + moneyFormat.format(premiumPerMember);
		this.premiumAmount = "$" + moneyFormat.format(premiumPerMember);
	}

	public ProductInfo() {
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(String premiumAmount) {
		this.premiumAmount = premiumAmount;
	}

	public String getSumAssured() {
		return sumAssured;
	}

	public void setSumAssured(String sumAssured) {
		this.sumAssured = sumAssured;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

}
