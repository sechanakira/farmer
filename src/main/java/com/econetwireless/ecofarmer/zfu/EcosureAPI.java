package com.econetwireless.ecofarmer.zfu;

public interface EcosureAPI {

	AuthenticationRequest checkAuth(AuthenticationRequest authenticationRequest);

	AuthenticationRequest checkReg(AuthenticationRequest authenticationRequest);

	ProductInfoList getProducts(AuthenticationRequest authenticationRequest);

	SubscriberInfo createEcoSureSubcription(SubscriberInfo subscriberInfo);

	boolean checkZFUREg(AuthenticationRequest authenticationRequest);

	AddOnRequest enableZFUAddOn(AddOnRequest addOnRequest);

	AddOnRequest disableZFUAddOn(AddOnRequest addOnRequest);

	AddOnRequest hasAddOn(AddOnRequest addOnRequest);

}
