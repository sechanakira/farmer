package com.econetwireless.ecofarmer.zfu;

public class AddOnRequest {

	private String addOnName;
	private String policyMsisdn;

	/**
	 * @return the addOnName
	 */
	public String getAddOnName() {
		return addOnName;
	}

	/**
	 * @param addOnName
	 *            the addOnName to set
	 */
	public void setAddOnName(String addOnName) {
		this.addOnName = addOnName;
	}

	/**
	 * @return the policyMsisdn
	 */
	public String getPolicyMsisdn() {
		return policyMsisdn;
	}

	/**
	 * @param policyMsisdn
	 *            the policyMsisdn to set
	 */
	public void setPolicyMsisdn(String policyMsisdn) {
		this.policyMsisdn = policyMsisdn;
	}

}
