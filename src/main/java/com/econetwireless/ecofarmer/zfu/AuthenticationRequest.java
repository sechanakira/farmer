package com.econetwireless.ecofarmer.zfu;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AuthenticationRequest {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 3553933319555077601L;

	@XmlElement(name = "msisdn", required = true, nillable = false)
	private String msisdn;
	private String pin;
	private boolean pinProvider;
	private String responseCode;
	private String responseMessage;
	private String merchantCode;
	private String merchantType;
	private String data;

	// @XmlElement(name = "countryCode", required = true, nillable = false)
	private String countryCode;

	@XmlElement(name = "acquirer", required = true, nillable = false)
	private String acquirer;

	private boolean dispatchNotification;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public AuthenticationRequest() {

	}

	public AuthenticationRequest(String msisdn, String pin, boolean pinProvider) {
		this.msisdn = msisdn;
		this.pin = pin;
		this.pinProvider = pinProvider;
	}

	public String getMsisdn() {
		if (msisdn == null) {
			return msisdn;
		}
		if (msisdn.length() > 9) {
			msisdn = msisdn.substring(msisdn.length() - 9);
		}
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public boolean isPinProvider() {
		return pinProvider;
	}

	public void setPinProvider(boolean pinProvider) {
		this.pinProvider = pinProvider;
	}

	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public boolean isDispatchNotification() {
		return dispatchNotification;
	}

	public void setDispatchNotification(boolean dispatchNotification) {
		this.dispatchNotification = dispatchNotification;
	}

	public String getMerchantType() {
		return merchantType;
	}

	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getAcquirer() {
		// return acquirer;
		return acquirer.replaceAll("[\\W_]+", "").toUpperCase();
	}

	public void setAcquirer(String acquirer) {
		// this.acquirer = acquirer.replaceAll("[^a-zA-Z0-9\\s]",
		// "").toUpperCase();
		this.acquirer = acquirer.replaceAll("[\\W_]+", "").toUpperCase();
	}

	@Override
	public String toString() {
		return "AuthenticationRequest [acquirer=" + acquirer + ", countryCode=" + countryCode + ", msisdn=" + msisdn + ", merchantCode=" + merchantCode + ", merchantType=" + merchantType + ", responseMessage=" + responseMessage + ", responseCode=" + responseCode + ", dispatchNotification=" + dispatchNotification + ", data=" + data + "]";
	}

}
