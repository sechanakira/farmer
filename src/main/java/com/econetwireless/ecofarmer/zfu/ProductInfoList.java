package com.econetwireless.ecofarmer.zfu;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "productSummaries", "responseCode", "responseMessage" })
@XmlRootElement(name = "productInfoList")
public class ProductInfoList {

	protected List<ProductInfoList.ProductSummaries> productSummaries;
	protected short responseCode;
	@XmlElement(required = true)
	protected String responseMessage;

	/**
	 * Gets the value of the productSummaries property.
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the productSummaries property.
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getProductSummaries().add(newItem);
	 * </pre>
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link ProductInfoList.ProductSummaries }
	 */
	public List<ProductInfoList.ProductSummaries> getProductSummaries() {
		if (productSummaries == null) {
			productSummaries = new ArrayList<ProductInfoList.ProductSummaries>();
		}
		return this.productSummaries;
	}

	/**
	 * Gets the value of the responseCode property.
	 */
	public short getResponseCode() {
		return responseCode;
	}

	/**
	 * Sets the value of the responseCode property.
	 */
	public void setResponseCode(short value) {
		this.responseCode = value;
	}

	/**
	 * Gets the value of the responseMessage property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getResponseMessage() {
		return responseMessage;
	}

	/**
	 * Sets the value of the responseMessage property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setResponseMessage(String value) {
		this.responseMessage = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="productId" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="productName" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="displayName" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="premiumAmount" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="sumAssured" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "productId", "productName", "displayName", "premiumAmount", "sumAssured" })
	public static class ProductSummaries {

		@XmlElement(required = true)
		protected String productId;
		@XmlElement(required = true)
		protected String productName;
		@XmlElement(required = true)
		protected String displayName;
		@XmlElement(required = true)
		protected String premiumAmount;
		@XmlElement(required = true)
		protected String sumAssured;

		/**
		 * Gets the value of the productId property.
		 * 
		 * @return possible object is {@link String }
		 */
		public String getProductId() {
			return productId;
		}

		/**
		 * Sets the value of the productId property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 */
		public void setProductId(String value) {
			this.productId = value;
		}

		/**
		 * Gets the value of the productName property.
		 * 
		 * @return possible object is {@link String }
		 */
		public String getProductName() {
			return productName;
		}

		/**
		 * Sets the value of the productName property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 */
		public void setProductName(String value) {
			this.productName = value;
		}

		/**
		 * Gets the value of the displayName property.
		 * 
		 * @return possible object is {@link String }
		 */
		public String getDisplayName() {
			return displayName;
		}

		/**
		 * Sets the value of the displayName property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 */
		public void setDisplayName(String value) {
			this.displayName = value;
		}

		/**
		 * Gets the value of the premiumAmount property.
		 * 
		 * @return possible object is {@link String }
		 */
		public String getPremiumAmount() {
			return premiumAmount;
		}

		/**
		 * Sets the value of the premiumAmount property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 */
		public void setPremiumAmount(String value) {
			this.premiumAmount = value;
		}

		/**
		 * Gets the value of the sumAssured property.
		 * 
		 * @return possible object is {@link String }
		 */
		public String getSumAssured() {
			return sumAssured;
		}

		/**
		 * Sets the value of the sumAssured property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 */
		public void setSumAssured(String value) {
			this.sumAssured = value;
		}

	}

}
