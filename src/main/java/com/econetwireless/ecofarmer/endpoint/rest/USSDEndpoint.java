package com.econetwireless.ecofarmer.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.econetwireless.ecofarmer.ussd.gateway.MessageRequestType;
import com.econetwireless.ecofarmer.ussd.gateway.MessageResponseType;
import com.econetwireless.ecofarmer.ussd.main.MainUSSDRequestProcessor;

@RestController
@RequestMapping("/endpoint")
public class USSDEndpoint {

	@Autowired
	private MainUSSDRequestProcessor mainUSSDRequestProcessor;

	@PostMapping(value = "/ussd", consumes = "application/xml", produces = "application/xml")
	public MessageResponseType processUSSDRequest(@RequestBody MessageRequestType ussdRequest) {
		return mainUSSDRequestProcessor.processUSSDRequest(ussdRequest);
	}
}
