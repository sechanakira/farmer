package com.econetwireless.ecofarmer.endpoint.rest;

import com.econetwireless.ecofarmer.dto.EcoFarmerRegistrationData;
import com.econetwireless.ecofarmer.dto.mapper.DistrictMapper;
import com.econetwireless.ecofarmer.dto.mapper.EcoFarmerRegistrationDataMapper;
import com.econetwireless.ecofarmer.dto.mapper.LanguageMapper;
import com.econetwireless.ecofarmer.dto.mapper.ProvinceMapper;
import com.econetwireless.ecofarmer.exception.UserNotRegisteredException;
import com.econetwireless.ecofarmer.persistence.model.EcoCashCustomerData;
import com.econetwireless.ecofarmer.persistence.model.Farmer;
import com.econetwireless.ecofarmer.persistence.service.EcoCashCustomerDataService;
import com.econetwireless.ecofarmer.persistence.service.FarmerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static java.util.Optional.ofNullable;

/**
 * Created by shingirai.chanakira on 2/19/2017.
 */
@RestController
@RequestMapping("/registration")
public class EcofarmerDataEndpoint {

    @Autowired
    private FarmerService farmerService;

    @Autowired
    private EcoCashCustomerDataService ecoCashCustomerDataService;

    @Autowired
    private EcoFarmerRegistrationDataMapper ecoFarmerRegistrationDataMapper;

    @Autowired
    private ProvinceMapper provinceMapper;

    @Autowired
    private DistrictMapper districtMapper;

    @Autowired
    private LanguageMapper languageMapper;

    @GetMapping(value = "/lookup/{msisdn}", produces = "application/json")
    private EcoFarmerRegistrationData getRegistrationData(@PathVariable("msisdn") String msisdn){
        Optional<Farmer> farmer = ofNullable(farmerService.findByMsisdn(msisdn.trim()));
        Optional<EcoCashCustomerData> ecoCashCustomerData = ofNullable(ecoCashCustomerDataService.findByMSISDN(msisdn));
        if(farmer.isPresent() && ecoCashCustomerData.isPresent()){
            return ecoFarmerRegistrationDataMapper.sourceToDestination(ecoCashCustomerData.get(),farmer.get(), provinceMapper, districtMapper, languageMapper);
        }
        throw new UserNotRegisteredException();
    }
}
