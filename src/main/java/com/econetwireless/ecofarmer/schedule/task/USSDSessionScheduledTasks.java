package com.econetwireless.ecofarmer.schedule.task;

import org.springframework.scheduling.annotation.Scheduled;

import com.econetwireless.ecofarmer.persistence.service.USSDSessionService;

public class USSDSessionScheduledTasks {

	private USSDSessionService ussdSessionService;

	public USSDSessionScheduledTasks(USSDSessionService ussdSessionService) {
		this.ussdSessionService = ussdSessionService;
	}

	@Scheduled(fixedDelay = 5 * 60 * 1000)
	public void expiredSessionCleaner() {
		ussdSessionService.removeExpiredSessions();
	}
}
