package com.econetwireless.ecofarmer.email;

import java.util.List;

@FunctionalInterface
public interface EmailBean {

	public void sendEmail(String message, String subject, List<String> recipients, List<String> ccList);

}
