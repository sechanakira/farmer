package com.econetwireless.ecofarmer.email;

import static com.econetwireless.ecofarmer.constants.ApplicationConstants.EmailConstants.EMAIL_SOURCE_ADDRESS;

import java.util.List;

import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.Async;

public class EmailBeanImpl implements EmailBean {

	private MailSender mailSender;

	public EmailBeanImpl(MailSender mailSender) {
		this.mailSender = mailSender;
	}

	@Async
	@Override
	public void sendEmail(String message, String subject, List<String> recipients, List<String> ccList) {
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
		String[] recips = new String[recipients.size()];
		String[] cc = new String[ccList.size()];
		simpleMailMessage.setFrom(EMAIL_SOURCE_ADDRESS);
		simpleMailMessage.setSubject(subject);
		simpleMailMessage.setText(message);
		recipients.toArray(recips);
		ccList.toArray(cc);
		simpleMailMessage.setTo(recips);
		simpleMailMessage.setCc(cc);
		mailSender.send(simpleMailMessage);
	}

}
